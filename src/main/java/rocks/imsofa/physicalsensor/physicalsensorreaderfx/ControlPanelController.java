/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx;

import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import rocks.imsofa.net.simplesocket.DefaultSimpleSocketImpl;
import rocks.imsofa.net.simplesocket.MessageCallback;
import rocks.imsofa.net.simplesocket.SimpleSocket;

/**
 * FXML Controller class
 *
 * @author lendle
 */
public class ControlPanelController implements Initializable, MessageCallback {

    private SimpleSocket ss = null;
    @FXML
    private ListView<String> ipList;
    private ExecutorService executorService = Executors.newFixedThreadPool(5);

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        ss = new DefaultSimpleSocketImpl(10001);
        ss.addMessageCallback(this);
        ss.startReceivingMessage();
        ipList.setItems(FXCollections.observableArrayList());
    }

    @FXML
    void buttonSaveAction(ActionEvent event) {
        for (String ip : getIpList()) {
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        ss.send(ip, 10000, "save");
                    } catch (Exception ex) {
                        Logger.getLogger(ControlPanelController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
        }
    }

    @FXML
    void buttonStartAction(ActionEvent event) {
        for (String ip : getIpList()) {
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        ss.send(ip, 10000, "start");
                    } catch (Exception ex) {
                        Logger.getLogger(ControlPanelController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
        }
    }

    @FXML
    void buttonStopAction(ActionEvent event) {
        try {
            for (String ip : getIpList()) {
                ss.send(ip, 10000, "stop");
            }
        } catch (Exception ex) {
            Logger.getLogger(ControlPanelController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void shutdown() {
        ss.stopReceivingMessage();
        executorService.shutdown();
    }

    @Override
    public synchronized void onMessage(String string, int i, String string1) {
        Platform.runLater(new Runnable() {
            public void run() {
                if (getIpList().contains(string1) == false) {
                    ipList.getItems().add(string1);
                }
            }
        });
    }

    @FXML
    void buttonRebootAction(ActionEvent event) {
        for (String ip : getIpList()) {
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        ss.send(ip, 10000, "reboot");
                    } catch (Exception ex) {
                        Logger.getLogger(ControlPanelController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
        }
    }

    @FXML
    void buttonShutdownAction(ActionEvent event) {
        for (String ip : getIpList()) {
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        ss.send(ip, 10000, "shutdown");
                    } catch (Exception ex) {
                        Logger.getLogger(ControlPanelController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
        }
    }
    
    private synchronized List<String> getIpList(){
        return Arrays.asList(ipList.getItems().toArray(new String[0]));
    }

}
