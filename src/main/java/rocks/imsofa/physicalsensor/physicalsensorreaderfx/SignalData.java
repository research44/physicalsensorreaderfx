/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx;

/**
 *
 * @author lendle
 */
  public class SignalData {
    private long timestamp=-1;
    private Object value=null;

    public SignalData(long timestamp, Object value) {
        this.timestamp=timestamp;
        this.value=value;
    }

    public SignalData() {
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "SignalData{" + "timestamp=" + timestamp + ", value=" + value + '}';
    }
    
    
    
}
