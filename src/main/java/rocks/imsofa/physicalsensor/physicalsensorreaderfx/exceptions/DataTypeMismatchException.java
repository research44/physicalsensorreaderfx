/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.exceptions;

/**
 *
 * @author lendle
 */
public class DataTypeMismatchException extends RuntimeException{
    private Class expectedType, currentType;
    public DataTypeMismatchException(Class expectedType, Class currentType) {
        super("Data type mismatch, expecting "+expectedType+", got "+currentType);
    }

    public Class getExpectedType() {
        return expectedType;
    }

    public void setExpectedType(Class expectedType) {
        this.expectedType = expectedType;
    }

    public Class getCurrentType() {
        return currentType;
    }

    public void setCurrentType(Class currentType) {
        this.currentType = currentType;
    }
    
    
    
}
