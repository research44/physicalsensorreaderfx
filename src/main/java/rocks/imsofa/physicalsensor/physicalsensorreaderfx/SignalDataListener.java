/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx;

import rocks.imsofa.physicalsensor.physicalsensorreaderfx.receiver.SignalDataReceiver;

/**
 *
 * @author lendle
 */
public interface SignalDataListener {
    public void newData(SignalDataReceiver source, SignalData data);
}
