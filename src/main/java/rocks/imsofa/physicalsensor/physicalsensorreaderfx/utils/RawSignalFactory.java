/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils;

import java.util.ArrayList;
import java.util.List;
import rocks.imsofa.pagedrecordlist.PagedRecordCollectionList;
import rocks.imsofa.pagedrecordlist.RecordEntry;
import rocks.imsofa.personality_assess.RawSignal;

/**
 *
 * @author USER
 */
public class RawSignalFactory {

    public static List<RawSignal> toRawSignal(PagedRecordCollectionList src, long interval) throws Exception {
        RecordEntry firstEntry = src.get(0);
        List<RawSignal> ret=new ArrayList<>();
        long intervalStart = Double.valueOf(""+firstEntry.getValue("timestamp")).longValue();
        double intervalSum = 0;
        long intervalCount=0;
        for (int i = 0; i < src.size(); i++) {
            RecordEntry entry = src.get(i);
            long timestamp = Double.valueOf(""+entry.getValue("timestamp")).longValue();
            Double value = (Double) entry.getValue("value");
            intervalSum+=value;
            intervalCount++;
            if(intervalStart==-1){
                intervalStart=timestamp;
            }
            else if((timestamp-intervalStart)>=interval){
                RawSignal rawSignal=new RawSignal(intervalStart, intervalSum/intervalCount);
                ret.add(rawSignal);
                intervalSum=0;
                intervalCount=0;
                intervalStart=Long.valueOf(-1);
            }
        }
        if(intervalSum!=0){
            RawSignal rawSignal=new RawSignal(intervalStart, intervalSum/intervalCount);
            ret.add(rawSignal);
        }
        return ret;
    }
}
