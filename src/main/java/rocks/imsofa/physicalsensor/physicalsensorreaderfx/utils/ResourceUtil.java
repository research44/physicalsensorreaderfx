/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.commons.io.IOUtils;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.App;

/**
 *
 * @author lendle
 */
public class ResourceUtil {
    public static String getTextFromResource(String resource) throws IOException{
        try(InputStream input=App.class.getResource(resource).openStream()){
            return IOUtils.toString(input, "utf-8");
        }
    }
    
    public static void getImageFromResource(File file, String resource) throws IOException{
        try(InputStream input=App.class.getResource(resource).openStream(); OutputStream output=new FileOutputStream(file)){
            IOUtils.copy(input, output);
        }
    }
}
