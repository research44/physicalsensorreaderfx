/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import rocks.imsofa.pagedrecordlist.PagedRecordCollectionList;
import rocks.imsofa.pagedrecordlist.RecordEntry;
import rocks.imsofa.personality_assess.Output;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.App;

/**
 *
 * @author lendle
 */
public class SerializationUtil {

    public static void toCSV(File csvFile, PagedRecordCollectionList list) throws Exception {
        FileUtils.write(csvFile, String.join(",", "timestamp", "value", "elapsed") + "\r\n", "utf-8");
        long startTime = -1;

        for (int i = 0; i < list.size(); i++) {
            RecordEntry recordEntry = list.get(i);
            long timestamp = Double.valueOf("" + recordEntry.getValue("timestamp")).longValue();
            double value = Double.valueOf("" + recordEntry.getValue("value"));
            if (startTime == -1) {
                startTime = Double.valueOf("" + recordEntry.getValue("timestamp")).longValue();
            }
            long elapsed = timestamp - startTime;
            String[] row = new String[]{
                "" + timestamp,
                "" + value,
                "" + elapsed
            };

            FileUtils.write(csvFile, String.join(",", row) + "\r\n", "utf-8", true);
        }
    }

    public static PagedRecordCollectionList fromCSV(File csvFile, String timestampColumn, String valueColumn, File backendFolder) throws Exception {
        Reader in = new FileReader(csvFile);
        PagedRecordCollectionList ret = PagedRecordCollectionList.newInstance(backendFolder);
        Iterable<CSVRecord> records = CSVFormat.EXCEL.withHeader().parse(in);
        for (CSVRecord record : records) {
            String timestampString = record.get(timestampColumn);
            String valueString = record.get(valueColumn);
            RecordEntry entry = new RecordEntry();
            entry.setValue("timestamp", Double.valueOf(timestampString).longValue());
            entry.setValue("value", Double.valueOf(valueString));
            ret.add(entry);
        }
        return ret;
    }

    public static PagedRecordCollectionList fromCSV(File csvFile) throws Exception {
        File tempBackendDirectory = createTempDirectory();
        return fromCSV(csvFile, "timestamp", "value", tempBackendDirectory);
    }

    public static File createTempDirectory() {
        File homeDirectory = new File(new File(System.getProperty("user.home")), ".physicalsensorreader");
        File tempDirectory = new File(homeDirectory, ".tmp");
        File tempBackendDirectory = new File(tempDirectory, "" + System.currentTimeMillis());
        tempBackendDirectory.mkdirs();
        return tempBackendDirectory;
    }

    public static PagedRecordCollectionList newPagedRecordCollectionList() throws Exception {
        PagedRecordCollectionList ret = PagedRecordCollectionList.newInstance(createTempDirectory());
        return ret;
    }

    public static PagedRecordCollectionList newPagedRecordCollectionList(File backendDirectory) throws Exception {
        if (!backendDirectory.exists()) {
            backendDirectory.mkdirs();
        }
        PagedRecordCollectionList ret = PagedRecordCollectionList.newInstance(backendDirectory);
        return ret;
    }

    private static void exportResourceToFolder(File rootExportDir, String resourceName) {
        File targetDir = null;
        if (resourceName.endsWith(".js")) {
            targetDir = new File(rootExportDir, "js");
        } else if (resourceName.endsWith(".css")) {
            targetDir = new File(rootExportDir, "css");
        } else {
            targetDir = new File(rootExportDir, "img");
        }
        if (!targetDir.exists()) {
            targetDir.mkdirs();
        }
        File targetFile = new File(targetDir, resourceName);
        try ( InputStream input = App.class.getResource(resourceName).openStream();  OutputStream output = new FileOutputStream(targetFile)) {
            IOUtils.copy(input, output);
        } catch (IOException ex) {
            Logger.getLogger(SerializationUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void exportReport(File targetDir, Map<String, PagedRecordCollectionList> dataMap, Output assessResult) {
        try {
            exportResourceToFolder(targetDir, "chart.min.js");
            exportResourceToFolder(targetDir, "bootstrap.min.css");
            exportResourceToFolder(targetDir, "agreeable.png");
            exportResourceToFolder(targetDir, "bg.png");
            exportResourceToFolder(targetDir, "conscientious.png");
            exportResourceToFolder(targetDir, "experience.png");
            exportResourceToFolder(targetDir, "extraverted.png");
            exportResourceToFolder(targetDir, "neurotic.png");
            exportResourceToFolder(targetDir, "bootstrap.bundle.min.js");
            exportResourceToFolder(targetDir, "jquery-3.6.0.min.js");
            TemplateEngine templateEngine = new TemplateEngine();
            ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
            templateResolver.setTemplateMode("HTML");
            templateEngine.setTemplateResolver(templateResolver);
            Context context = new Context();
            Map reportData = new HashMap();
            List charts = new ArrayList();
            reportData.put("charts", charts);
            for (String id : dataMap.keySet()) {
                if (id.equals("hr") == false && id.equals("test3") == false) {
                    continue;
                }
                try {
                    PagedRecordCollectionList list = dataMap.get(id);
                    Map chart = new HashMap();
                    chart.put("name", id);
                    chart.put("labels", getTimestampAsElapsed(list));
                    chart.put("values", getValue(list));
                    chart.put("color", "red");
                    charts.add(chart);
                } catch (Exception ex) {
                    Logger.getLogger(SerializationUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            Map assessResults = new HashMap();
            if(assessResult!=null){
                reportData.put("assessResults", assessResults);
                Map exMap = new HashMap();
                Map agMap = new HashMap();
                Map coMap = new HashMap();
                Map esMap = new HashMap();
                Map opMap = new HashMap();
                assessResults.put("ex", exMap);
                assessResults.put("ag", agMap);
                assessResults.put("co", coMap);
                assessResults.put("es", esMap);
                assessResults.put("op", opMap);
                if (assessResult.getExClassResults().get(0).getLevel().equals(rocks.imsofa.personality_assess.Level.HIGH)) {
                    exMap.put("text", ResourceUtil.getTextFromResource("Ex_High.txt"));
                    exMap.put("level", "HIGH");
                } else {
                    exMap.put("text", ResourceUtil.getTextFromResource("Ex_Low.txt"));
                    exMap.put("level", "LOW");
                }

                if (assessResult.getAgClassResults().get(0).getLevel().equals(rocks.imsofa.personality_assess.Level.HIGH)) {
                    agMap.put("text", ResourceUtil.getTextFromResource("Ag_High.txt"));
                    agMap.put("level", "HIGH");
                } else {
                    agMap.put("text", ResourceUtil.getTextFromResource("Ag_Low.txt"));
                    agMap.put("level", "LOW");
                }

                if (assessResult.getCoClassResults().get(0).getLevel().equals(rocks.imsofa.personality_assess.Level.HIGH)) {
                    coMap.put("text", ResourceUtil.getTextFromResource("Co_High.txt"));
                    coMap.put("level", "HIGH");
                } else {
                    coMap.put("text", ResourceUtil.getTextFromResource("Co_Low.txt"));
                    coMap.put("level", "LOW");
                }

                if (assessResult.getEsClassResults().get(0).getLevel().equals(rocks.imsofa.personality_assess.Level.HIGH)) {
                    esMap.put("text", ResourceUtil.getTextFromResource("Es_High.txt"));
                    esMap.put("level", "HIGH");
                } else {
                    esMap.put("text", ResourceUtil.getTextFromResource("Es_Low.txt"));
                    esMap.put("level", "LOW");
                }

                if (assessResult.getOpClassResults().get(0).getLevel().equals(rocks.imsofa.personality_assess.Level.HIGH)) {
                    opMap.put("text", ResourceUtil.getTextFromResource("Op_High.txt"));
                    opMap.put("level", "HIGH");
                } else {
                    opMap.put("text", ResourceUtil.getTextFromResource("Op_Low.txt"));
                    opMap.put("level", "LOW");
                }
            }

            context.setVariable("reportData", reportData);
            StringWriter stringWriter = new StringWriter();
            templateResolver.setPrefix("template/");
            templateResolver.setSuffix(".html");
            templateResolver.setCharacterEncoding("utf-8");
            if(assessResult==null){
                templateEngine.process("indexWithoutAssessResults", context, stringWriter);
            }else{
                System.out.println("here");
                templateEngine.process("index", context, stringWriter);
            }
            FileUtils.write(new File(targetDir, "index.html"), stringWriter.toString(), "utf-8");
        } catch (IOException ex) {
            Logger.getLogger(SerializationUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static long[] getTimestampAsElapsed(PagedRecordCollectionList list) throws Exception {
        long[] ret = new long[(int) list.size()];
        long startTime = -1;
        for (int i = 0; i < list.size(); i++) {
            RecordEntry recordEntry = list.get(i);
            long timestamp = Double.valueOf("" + recordEntry.getValue("timestamp")).longValue();
            double value = Double.valueOf("" + recordEntry.getValue("value"));
            if (startTime == -1) {
                startTime = Double.valueOf("" + recordEntry.getValue("timestamp")).longValue();
            }
            long elapsed = timestamp - startTime;
            ret[i] = elapsed;
        }
        return ret;
    }

    private static double[] getValue(PagedRecordCollectionList list) throws Exception {
        double[] ret = new double[(int) list.size()];
        for (int i = 0; i < list.size(); i++) {
            RecordEntry recordEntry = list.get(i);
            double value = Double.valueOf("" + recordEntry.getValue("value"));
            ret[i] = value;
        }
        return ret;
    }
}
