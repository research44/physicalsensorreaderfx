/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import rocks.imsofa.pagedrecordlist.PagedRecordCollectionList;
import rocks.imsofa.pagedrecordlist.RecordEntry;
import rocks.imsofa.personality_assess.AssessorFactory;
import rocks.imsofa.personality_assess.Input;
import rocks.imsofa.personality_assess.InputFactory;
import rocks.imsofa.personality_assess.Output;
import rocks.imsofa.personality_assess.RawSignal;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.DataMap;

/**
 *
 * @author lendle
 */
public class AssessUtil {

    private final static long MIN_INTERVAL = 7 * 60 * 1000;
    
    public static boolean verify(DataMap dataMap) throws Exception{
        PagedRecordCollectionList gsrList = dataMap.getGSRPagedRecordCollectionList();
        PagedRecordCollectionList hrList = dataMap.getHrPagedRecordCollectionList();
        List<RawSignal> gsrSignals = RawSignalFactory.toRawSignal(gsrList, 30000);
        List<RawSignal> hrSignals = RawSignalFactory.toRawSignal(hrList, 30000);
        if (getTotalInterval(gsrSignals) < MIN_INTERVAL || getTotalInterval(hrSignals) < MIN_INTERVAL) {
            return false;
        }else{
            return true;
        }
    }

    public static Output assess(DataMap dataMap, boolean useTestData) throws Exception {
        PagedRecordCollectionList gsrList = dataMap.getGSRPagedRecordCollectionList();
        PagedRecordCollectionList hrList = dataMap.getHrPagedRecordCollectionList();
        if (useTestData) {
            long startTime = System.currentTimeMillis();
            for (int i = 0; i < 1000; i++) {
                RecordEntry gsr = new RecordEntry();
                gsr.setValue("timestamp", startTime + i * 10000);
                gsr.setValue("value", 300 + 500 * Math.random());
                gsrList.add(gsr);

                RecordEntry hr = new RecordEntry();
                hr.setValue("timestamp", startTime + i * 10000);
                hr.setValue("value", 80 + 100 * Math.random());
                hrList.add(hr);

            }
        }
        List<RawSignal> gsrSignals = RawSignalFactory.toRawSignal(gsrList, 30000);
        List<RawSignal> hrSignals = RawSignalFactory.toRawSignal(hrList, 30000);
        
        Input input = InputFactory.createInput(gsrSignals, hrSignals, true);
        AssessorFactory assessorFactory = new AssessorFactory();
        Output output = assessorFactory.newAssessor().execute(input);
        return output;
    }
    

    private static long getTotalInterval(List<RawSignal> values) {
        return values.get(values.size() - 1).getTimestamp() - values.get(0).getTimestamp();
    }
}
