/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.client.Client;

/**
 *
 * @author lendle
 */
public class PropertiesUtil {

    private static Properties props = null;

    public synchronized static Properties getProperties() {
        if (props == null) {
            File confFile = new File(getConfFolder(), "properties.conf");
            props = new Properties();
            if (!confFile.exists()) {
                return props;
            }
            try (InputStream input = new FileInputStream(confFile)) {
                props.load(input);
            } catch (IOException ex) {
                Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return props;
    }

    protected static File getConfFolder() {
        File file = new File(".conf");
        if (!file.exists() || !file.isDirectory()) {
            file.mkdirs();
        }
        return file;
    }
    
    public static boolean isTestMode(){
        Properties props=getProperties();
        return "true".equals(props.getProperty("testMode"));
    }
    
    public static String getIdPrefix(){
        Properties props=getProperties();
        return props.getProperty("idPrefix");
    }
    
    public static String getNetworkInterface(){
        Properties props=getProperties();
        return props.getProperty("networkInterface");
    }
    
    public synchronized static void writeIdPrefix(String idPrefix){
        Properties props=getProperties();
        props.setProperty("idPrefix", idPrefix);
        File targetFile=new File(getConfFolder(), "properties.conf");
        try(OutputStream output=new FileOutputStream(targetFile)){
            props.store(output, "");
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
