/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 *
 * @author lendle
 */
public class ThreadUtil {
    private static ExecutorService executorService = null;
    static{
        executorService = Executors.newFixedThreadPool(20);
    }
    
    public static ExecutorService getExecutorService(){
        return executorService;
    }
    
    public synchronized static Future submit(Runnable r){
        return executorService.submit(r);
    }
    
    public static void shutdownNow(){
        executorService.shutdownNow();
    }
}
