/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lendle
 */
public class IPUtil {

    public static String getIntranetIP() {
        try {
            List<String> candidates = new ArrayList<>();
            String ip = null;
            Enumeration<NetworkInterface> enumeration = NetworkInterface.getNetworkInterfaces();
            String niName=PropertiesUtil.getNetworkInterface();
            outer:
            while (enumeration.hasMoreElements()) {
                NetworkInterface networkInterface = enumeration.nextElement();
                if(niName!=null && niName.equals(networkInterface.getDisplayName())==false){
                    continue;
                }
//                System.out.println(networkInterface.getDisplayName());
                Enumeration<InetAddress> addresses = networkInterface.getInetAddresses();
                while (addresses.hasMoreElements()) {
                    InetAddress address = addresses.nextElement();
//                    Logger.getLogger(IPUtil.class.getName()).info("testing: " + address.getHostAddress());
//                    System.out.println("testing: " + address.getHostAddress());
                    if (address.getHostAddress().startsWith("192") || address.getHostAddress().startsWith("10.0")) {
                        if (address.getHostAddress().startsWith("192.168.0") || address.getHostAddress().startsWith("192.168.1.") || address.getHostAddress().startsWith("10.0")) {
                            ip = address.getHostAddress();
                            candidates.add(address.getHostAddress());
                            break outer;
                        }
                        candidates.add(address.getHostAddress());
                    }
                }
            }
//            System.out.println("ip=" + ip);
            if(candidates.isEmpty()){
                return null;
            }
            return (ip != null) ? ip : candidates.get(0);
        } catch (SocketException ex) {
            Logger.getLogger(IPUtil.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static void main(String[] args) throws Exception {
        System.out.println(IPUtil.getIntranetIP());
    }
}
