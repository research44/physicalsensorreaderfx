/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.charts;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import rocks.imsofa.pagedrecordlist.PagedRecordCollectionList;
import rocks.imsofa.pagedrecordlist.RecordEntry;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.PrimaryController;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalData;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalDataListener;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.receiver.SignalDataReceiver;

/**
 *
 * @author lendle
 */
public class SignalDataLineChart extends LineChart<Number, Number> implements SignalDataListener {

    protected XYChart.Series series = new XYChart.Series();
    private SignalDataReceiver signalDataReceiver = null;
    private List<SignalData> pendingQueue=new ArrayList<>();//to make the display more smooth, display of new data will be pending
    long startTime = -1;

    public SignalDataLineChart() {
        super(new NumberAxis(), new NumberAxis());
        this.getXAxis().setAutoRanging(false);
        this.getData().add(series);
        this.setCreateSymbols(false);
        Thread pendingQueueConsumptionDaemon=new Thread(){
            public void run(){
                while(true){
                    try {
                        Thread.sleep(100);
                        synchronized(SignalDataLineChart.this){
                            if(!pendingQueue.isEmpty()){
                                SignalData data=pendingQueue.remove(0);
                                addData(data);
                            }
                        }
                    } catch (InterruptedException ex) {
                        Logger.getLogger(SignalDataLineChart.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        };
        pendingQueueConsumptionDaemon.setDaemon(true);
        pendingQueueConsumptionDaemon.start();
    }

    public void setSignalDataReceiver(SignalDataReceiver signalDataReceiver){
        if(this.signalDataReceiver!=null){
            this.signalDataReceiver.removeDataListener(this);
        }
        this.signalDataReceiver=signalDataReceiver;
        this.signalDataReceiver.addDataListener(this);
    }
    
    public Series getSeries() {
        return series;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    @Override
    public void newData(SignalDataReceiver source, SignalData data) {
//        addData(data);
        pendingQueue.add(data);
    }

    protected void addData(SignalData data) {
        DecimalFormat df = new DecimalFormat("#.#");
        if (startTime == -1) {
            startTime = System.currentTimeMillis();
        }
        Platform.runLater(() -> {
            double second10th = Double.valueOf(df.format((double) ((data.getTimestamp() - startTime) / 1000.0)));
//            System.out.println(second10th + ":" + data.getTimestamp() + ":" + startTime);
            if (series.getData().size() > 100) {
                series.getData().remove(0);
                NumberAxis xAxis = (NumberAxis) this.getXAxis();
                xAxis.setLowerBound(((Number) ((XYChart.Data) series.getData().get(0)).getXValue()).doubleValue());
                xAxis.setUpperBound(second10th);
//                System.out.println(xAxis.getLowerBound() + ":" + xAxis.getUpperBound());
            }
            if (!series.getData().isEmpty()) {
                XYChart.Data lastData = (XYChart.Data) series.getData().get(series.getData().size() - 1);
                if (lastData.getXValue().equals(second10th)) {
                    double original = ((Double) lastData.getYValue()).doubleValue();
                    lastData.setYValue((Double) ((original + (Double) data.getValue()) / 2));
                } else {
                    series.getData().add(new XYChart.Data(second10th, (Double) data.getValue()));
                }
            } else {
                series.getData().add(new XYChart.Data(second10th, (Double) data.getValue()));
            }
        });
    }

    public void loadFromPagedRecordCollectionList(PagedRecordCollectionList list) {
        if(list==null){
            return;
        }
        this.getSeries().getData().clear();
        for (int i = 0; i < list.size(); i++) {
            try {
                RecordEntry entry = list.get(i);
                Long timestamp = Double.valueOf(""+entry.getValue("timestamp")).longValue();
                Double value = (Double) entry.getValue("value");
                if (i == 0) {
                    this.setStartTime(timestamp);
                }
                SignalData signalData = new SignalData(timestamp, value);
                this.addData(signalData);
            } catch (Exception ex) {
                Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void clear(){
        series.getData().clear();
    }
}
