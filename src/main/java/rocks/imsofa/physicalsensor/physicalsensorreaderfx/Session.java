/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx;

import java.io.File;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import rocks.imsofa.pagedrecordlist.PagedRecordCollectionList;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.receiver.SignalDataReceiver;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.SerializationUtil;

/**
 *
 * @author lendle
 */
public class Session {

    protected PhysicalSensorReaderModel physicalSensorReaderModel = null;
    /**
     * receiver id=>list
     */
    protected DataMap dataMap = new DataMap();
    protected File projectRootDirectory = null;

    public DataMap getDataMap() {
        return dataMap;
    }

    public PhysicalSensorReaderModel getModel(){
        return physicalSensorReaderModel;
    }
    
    public void init(File projectRootDirectory) throws Exception {
        this.projectRootDirectory = projectRootDirectory;
        this.physicalSensorReaderModel = new PhysicalSensorReaderModel(this.projectRootDirectory);
        dataMap.setFrequenciesPageRecordCollectionList(SerializationUtil.newPagedRecordCollectionList(new File(projectRootDirectory, "frequencies_data")));
        dataMap.setVolumesPageRecordCollectionList(SerializationUtil.newPagedRecordCollectionList(new File(projectRootDirectory, "volumes_data")));
        dataMap.setHrPageRecordCollectionList(SerializationUtil.newPagedRecordCollectionList(new File(projectRootDirectory, "hr_data")));
        dataMap.setGSRPageRecordCollectionList(PagedRecordCollectionList.newInstance(new File(projectRootDirectory, "gsr_data")));
    }

    public void addDataListener(SignalDataType type, SignalDataListener listener) {
        switch (type) {
            case TYPE_FREQUENCY:
                physicalSensorReaderModel.getFrequencySignalDataReceiver().addDataListener(listener);
                break;
            case TYPE_GSR:
                physicalSensorReaderModel.getGSRSignalDataReceiver().addDataListener(listener);
                break;
            case TYPE_HR:
                physicalSensorReaderModel.getHrSignalDataReceiver().addDataListener(listener);
                break;
            case TYPE_VOLUME:
                physicalSensorReaderModel.getVolumesSignalDataReceiver().addDataListener(listener);
                break;
            default:
                throw new AssertionError();
        }
    }

    public SignalDataReceiver getSignalDataReceiver(SignalDataType signalDataType) {
        switch (signalDataType) {
            case TYPE_FREQUENCY:
                return physicalSensorReaderModel.getFrequencySignalDataReceiver();
            case TYPE_GSR:
                return physicalSensorReaderModel.getGSRSignalDataReceiver();
            case TYPE_HR:
                return physicalSensorReaderModel.getHrSignalDataReceiver();
            case TYPE_VOLUME:
                return physicalSensorReaderModel.getVolumesSignalDataReceiver();
            default:
                throw new AssertionError();
        }
    }

    public void removeDataListener(SignalDataListener listener) {
        physicalSensorReaderModel.getFrequencySignalDataReceiver().removeDataListener(listener);
        physicalSensorReaderModel.getGSRSignalDataReceiver().removeDataListener(listener);
        physicalSensorReaderModel.getHrSignalDataReceiver().removeDataListener(listener);
        physicalSensorReaderModel.getVolumesSignalDataReceiver().removeDataListener(listener);
    }

    public synchronized void removeAllDataListeners(){
        physicalSensorReaderModel.getFrequencySignalDataReceiver().removeAllDataListeners();
        physicalSensorReaderModel.getGSRSignalDataReceiver().removeAllDataListeners();
        physicalSensorReaderModel.getHrSignalDataReceiver().removeAllDataListeners();
        physicalSensorReaderModel.getVolumesSignalDataReceiver().removeAllDataListeners();
    }
    
    public void start(boolean fakeData) throws Exception {
        this.physicalSensorReaderModel.start(fakeData);
    }
    
    public void start() throws Exception {
        start(false);
    }
    
    public void shutdown() throws Exception {
        this.dataMap.shutdown();
    }

    public void stop() throws Exception {
        this.physicalSensorReaderModel.stop();
    }

    public void save() throws Exception {
        this.dataMap.save(projectRootDirectory);
    }
    
    public PagedRecordCollectionList getPagedRecordCollectionList(SignalDataType type){
        switch (type) {
            case TYPE_FREQUENCY:
                return dataMap.getFrequenciesPagedRecordCollectionList();
            case TYPE_GSR:
                return dataMap.getGSRPagedRecordCollectionList();
            case TYPE_HR:
                return dataMap.getHrPagedRecordCollectionList();
            case TYPE_VOLUME:
                return dataMap.getVolumesPagedRecordCollectionList();
            default:
                throw new AssertionError();
        }
    }

    public void load(File dir) {
        if (dir != null) {
            List<String> keys = dataMap.getDataMapKeys();
            for (String key : keys) {
                File csvFile = new File(dir, "" + key + ".csv");
                if (csvFile.exists()) {
                    try {
                        PagedRecordCollectionList list = SerializationUtil.fromCSV(csvFile);
                        dataMap.setPageRecordCollectionList(key, list);
                    } catch (Exception ex) {
                        Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }
}
