/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import rocks.imsofa.net.simplesocket.DefaultSimpleSocketImpl;
import rocks.imsofa.net.simplesocket.SimpleSocket;

/**
 *
 * @author lendle
 */
public class ControlPanel extends Application{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception{
        // TODO code application logic here
        launch();
        
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("ControlPanel.fxml"));
        Parent root=fxmlLoader.load();
        ControlPanelController controller=fxmlLoader.getController();
        Scene scene = new Scene((Parent) root, 250, 400);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Control Panel");
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>(){
            @Override
            public void handle(WindowEvent event) {
                controller.shutdown();
            }
        });
        primaryStage.show();
//        SimpleSocket ss=new DefaultSimpleSocketImpl();
//        ss.send("localhost", 10000, "start");
//        Thread.sleep(3000);
//        ss.send("localhost", 10000, "stop");
//        Thread.sleep(3000);
//        ss.send("localhost", 10000, "save");
    }
    
}
