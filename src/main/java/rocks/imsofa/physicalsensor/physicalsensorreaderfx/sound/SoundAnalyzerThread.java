/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.sound;

import com.darkprograms.speech.microphone.MicrophoneAnalyzer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalData;

/**
 *
 * @author lendle
 */
public class SoundAnalyzerThread extends Thread {

    private TargetDataLine line = null;
    private boolean running = true;
    private List<SignalData> volumes = new ArrayList<>(), frequencies = new ArrayList<>();
    private boolean showOutput = false;
    private boolean daemon = true;

    public SoundAnalyzerThread() {
        this(false, true);
    }

    public SoundAnalyzerThread(boolean showOutput, boolean daemon) {
        this.showOutput = showOutput;
        this.setDaemon(daemon);
    }

    public List<SignalData> getVolumes() {
        return volumes;
    }

    public List<SignalData> getFrequencies() {
        return frequencies;
    }

    public void shutdown() {
        line.stop();
        line.close();
        running = false;
    }

    public void run() {
        try {
            float sampleRate = 16000;
            int sampleSizeInBits = 8;
            int channels = 2;
            boolean signed = true;
            boolean bigEndian = true;
            Object lock = new Object();
            List<Byte> buffer = new ArrayList<>();
            AudioFormat format = new AudioFormat(sampleRate, sampleSizeInBits,
                    channels, signed, bigEndian);
            DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
            if (!AudioSystem.isLineSupported(info)) {
                System.out.println("Line not supported");
                System.exit(0);
            }
            line = (TargetDataLine) AudioSystem.getLine(info);
            line.open(format);
            line.start();   // start capturing
            System.out.println("Start capturing...");

            AudioInputStream ais = new AudioInputStream(line);
            MicrophoneAnalyzer analyzer = new MicrophoneAnalyzer(AudioFileFormat.Type.WAVE);
            Thread recorder = new Thread() {
                public void run() {
                    byte[] smallBuffer = new byte[2];
                    while (running) {
                        try {
                            ais.read(smallBuffer);
                            synchronized (lock) {
                                for (int i = 0; i < 2; i++) {
                                    buffer.add(Byte.valueOf(smallBuffer[i]));
                                }
                                lock.notifyAll();
                            }
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            };
            recorder.setDaemon(true);
            recorder.start();
            while (running) {
                byte[] smallBuffer = null;
                synchronized (lock) {
                    while (buffer.size() < 2049) {
                        try {
                            lock.wait();
                        } catch (InterruptedException ex) {
                            ex.printStackTrace();
                        }
                    }
                    //estimate the closest power of 2 to the current buffer size
                    int n=log2(buffer.size()-1);
                    int N=(int) Math.pow(2, n)+1;
                    smallBuffer=new byte[N];
//                    System.out.println(N);
                    for (int i = 0; i < N; i++) {
                        smallBuffer[i] = buffer.remove(0);
                    }
                    buffer.clear();
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
                int frequency = analyzer.getFrequency(smallBuffer);
                int volume = analyzer.calculateRMSLevel(smallBuffer);
                synchronized (this) {
                    long timestamp = System.currentTimeMillis();
                    SignalData frequencySignalData = new SignalData(timestamp, Double.valueOf(frequency));
                    SignalData volumeSignalData = new SignalData(timestamp, Double.valueOf(volume));
                    this.frequencies.add(frequencySignalData);
                    this.volumes.add(volumeSignalData);
                    this.notifyAll();
                }
                if (this.showOutput) {
                    System.out.println(volume + ":" + frequency+":"+buffer.size());
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private int log2(int n){
        int result = (int)(Math.log(n) / Math.log(2));
        return result;
    }
    
    public static void main(String[] args) throws Exception {
        new SoundAnalyzerThread(true, false).start();
    }
}
