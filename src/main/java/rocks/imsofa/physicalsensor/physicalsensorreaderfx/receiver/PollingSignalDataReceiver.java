/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.receiver;

import java.util.logging.Level;
import java.util.logging.Logger;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalData;

/**
 *
 * @author lendle
 */
public abstract class PollingSignalDataReceiver extends AbstractSignalDataReceiver{
    protected long waitingInterval=1000;
    protected PollingThread pollingThread=null;
    protected FakeDataGenerator fakeDataGenerator=null;
    
    
    public PollingSignalDataReceiver(long waitingInterval) {
        this.waitingInterval=waitingInterval;
    }

    public PollingSignalDataReceiver() {
    }
    
    public long getWaitingInterval() {
        return waitingInterval;
    }

    public void setWaitingInterval(long waitingInterval) {
        this.waitingInterval = waitingInterval;
    }
    
    @Override
    public synchronized void start() {
        if(pollingThread==null){
            pollingThread=new PollingThread();
            pollingThread.setDaemon(true);
            pollingThread.start();
        }
    }

    public void start(FakeDataGenerator fakeDataGenerator){
        this.fakeDataGenerator=fakeDataGenerator;
        start();
    }

    @Override
    public synchronized void stop() {
        fakeDataGenerator=null;
        if(pollingThread!=null){
            pollingThread.shutdown();
            pollingThread=null;
        }
    }
    
    public abstract SignalData acquireData();
    
    class PollingThread extends Thread{
        private boolean running=true;
        public void shutdown(){
            this.running=false;
        }
        public void run(){
            while(running){
                try {
                    Thread.sleep(waitingInterval);
                } catch (InterruptedException ex) {
                    Logger.getLogger(FakeSignalDataReceiver.class.getName()).log(Level.SEVERE, null, ex);
                }
                SignalData data=(fakeDataGenerator!=null)?fakeDataGenerator.generateData():acquireData();
//                System.out.println(data);
                if(data!=null){
                    fireEvent(data);
                }
            }
        }
    }
}
