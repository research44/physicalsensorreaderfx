/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.receiver;

import rocks.imsofa.physicalsensor.physicalsensorreaderfx.receiver.PollingSignalDataReceiver;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalData;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalData;

/**
 *
 * @author lendle
 */
public class FakeSignalDataReceiver extends PollingSignalDataReceiver{
    public FakeSignalDataReceiver() {
        super(1000);
    }
    
    @Override
    public SignalData acquireData() {
        return new SignalData(System.currentTimeMillis(), Math.random()*100);
    }
    
}
