/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.receiver;

import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalDataListener;

/**
 * read signal data from underlying devices
 * @author lendle
 */
public interface SignalDataReceiver {
    public String getId();
    public void addDataListener(SignalDataListener signalDataListener);
    public void removeDataListener(SignalDataListener signalDataListener);
    public void start();
    public void stop();
    public Class getType();
}
