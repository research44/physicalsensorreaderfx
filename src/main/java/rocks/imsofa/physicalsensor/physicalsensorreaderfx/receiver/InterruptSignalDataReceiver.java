/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.receiver;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalData;

/**
 * a signal data receiver that monitors a given monitored queue via java's
 * monitor framework
 *
 * @author lendle
 */
public class InterruptSignalDataReceiver extends AbstractSignalDataReceiver {

    private List<SignalData> monitoredQueue = new Vector<>();
    private InterruptMonitoringThread interruptMonitoringThread = null;
    private Object lock = this;
    protected FakeDataGeneratorThread fakeDataGeneratorThread = null;
    private long lastMessageTimestamp=-1;

    public InterruptSignalDataReceiver(Class type) {
        super(type);
    }

    public InterruptSignalDataReceiver() {
    }

    public InterruptSignalDataReceiver(Class type, List<SignalData> monitoredQueue, Object lock) {
        super(type);
        this.monitoredQueue = monitoredQueue;
        this.lock = lock;
    }

    public InterruptSignalDataReceiver(List<SignalData> monitoredQueue, Object lock) {
        this.monitoredQueue = monitoredQueue;
        this.lock = lock;
    }

    public Object getLock() {
        return lock;
    }

    public void setLock(Object lock) {
        this.lock = lock;
    }

    @Override
    public void start() {
        synchronized (lock) {
            monitoredQueue.clear();//drop messages before start
        }

        if (interruptMonitoringThread == null) {
            interruptMonitoringThread = new InterruptMonitoringThread();
            interruptMonitoringThread.start();
        }
    }

    public void start(FakeDataGeneratorThread fakeDataGeneratorThread) {
        this.fakeDataGeneratorThread = fakeDataGeneratorThread;
        this.fakeDataGeneratorThread.setLock(lock);
        this.fakeDataGeneratorThread.setMonitoredQueue(monitoredQueue);
        this.fakeDataGeneratorThread.start();
        start();
    }

    @Override
    public void stop() {
        if (fakeDataGeneratorThread != null) {
            fakeDataGeneratorThread.shutdown();
            fakeDataGeneratorThread = null;
        }
        if (interruptMonitoringThread != null) {
            interruptMonitoringThread.shutdown();
            interruptMonitoringThread = null;
        }
    }

    private SignalData getSignalData() throws InterruptedException {
        synchronized (lock) {
            while (monitoredQueue.isEmpty()) {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                }
            }
            if (monitoredQueue.isEmpty() == false) {
                return monitoredQueue.remove(0);
            } else {
                return null;
            }
        }
    }

    public void putSignalData(SignalData signalData) {
        synchronized (lock) {
            this.monitoredQueue.add(signalData);
            lock.notifyAll();
        }
    }

    class InterruptMonitoringThread extends Thread {

        private boolean running = true;

        public InterruptMonitoringThread() {
            this.setDaemon(true);
        }

        public void shutdown() {
            this.running = false;
            this.interrupt();
        }

        public void run() {
            while (running) {

                synchronized (lock) {
                    try {
                        SignalData signalData = getSignalData();
                        long timestamp=signalData.getTimestamp();
                        if(timestamp<=lastMessageTimestamp){
                            continue;
                        }
                        lastMessageTimestamp=timestamp;
                        fireEvent(signalData);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(InterruptSignalDataReceiver.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }
        }
    }
}
