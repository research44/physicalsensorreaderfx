/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.receiver;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import jep.Interpreter;
import jep.JepException;
import jep.SharedInterpreter;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalData;

/**
 * a signal data receiver that collects data
 * from jep modules which in turn read from
 * python applications
 * @author lendle
 */
public class JepSignalDataReceiver extends InterruptSignalDataReceiver {
    private static Object staticLock = new Object();
    private static Interpreter interpreter = null;
    private static Map<String, List<SignalData>> queueMap=new HashMap<>();

    public JepSignalDataReceiver(String id) {
        super(Double.class, getQueue(id), staticLock);
    }

    public JepSignalDataReceiver(File pythonFile, String id) {
        super(Double.class, getQueue(id), staticLock);
        initInterpreter(pythonFile);
    }
    
    private synchronized static List<SignalData> getQueue(String id){
        List<SignalData> queue=queueMap.get(id);
        if(queue==null){
            queue=new Vector<>();
            queueMap.put(id, queue);
        }
        return queue;
    }

    private void initInterpreter(File pythonFile) {
        LOG.info("starting JepSignalDataReceiver with python file: "+pythonFile);
        synchronized (staticLock) {
            if (interpreter == null) {
                //initialize the process
                Thread t = new Thread() {
                    public void run() {
                        try {
                            interpreter = new SharedInterpreter();
                            interpreter.runScript(pythonFile.getAbsolutePath());
                        } catch (JepException ex) {
                            Logger.getLogger(JepSignalDataReceiver.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                };
                t.setDaemon(true);
                t.start();

            }
        }
    }
    private static final Logger LOG = Logger.getLogger(JepSignalDataReceiver.class.getName());
}
