/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.receiver;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalData;

/**
 *
 * @author lendle
 */
public class FakeDataGeneratorThread extends Thread{
    private boolean running=true;
    private List<SignalData> monitoredQueue=null;
    private long interval=1000;
    private FakeDataGenerator fakeDataGenerator=null;
    private Object lock=null;
    
    public FakeDataGeneratorThread(FakeDataGenerator fakeDataGenerator, List<SignalData> monitoredQueue, Object lock, long interval){
        this.fakeDataGenerator=fakeDataGenerator;
        this.monitoredQueue=monitoredQueue;
        this.interval=interval;
        this.lock=lock;
        this.setDaemon(true);
    }

    public FakeDataGeneratorThread(FakeDataGenerator fakeDataGenerator, List<SignalData> monitoredQueue, long interval){
        this(fakeDataGenerator, monitoredQueue, null, interval);
    }
    
    public FakeDataGeneratorThread(FakeDataGenerator fakeDataGenerator, long interval){
        this(fakeDataGenerator, null, null, interval);
    }

    public List<SignalData> getMonitoredQueue() {
        return monitoredQueue;
    }

    public void setMonitoredQueue(List<SignalData> monitoredQueue) {
        this.monitoredQueue = monitoredQueue;
    }
    
    
    
    public long getInterval() {
        return interval;
    }

    public void setInterval(long interval) {
        this.interval = interval;
    }

    public Object getLock() {
        return lock;
    }

    public void setLock(Object lock) {
        this.lock = lock;
    }
    
    
    
    public void run(){
        while(running){
            try {
                Thread.sleep(interval);
                synchronized (lock) {
                    SignalData data=this.fakeDataGenerator.generateData();
                    monitoredQueue.add(data);
                    lock.notifyAll();
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(FakeDataGeneratorThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void shutdown(){
        this.running=false;
        this.interrupt();
    }
}
