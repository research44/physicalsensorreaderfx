/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.receiver;

import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalData;

/**
 *
 * @author lendle
 */
public class SimpleFakeDataGenerator implements FakeDataGenerator{
    private double baseValue=0;
    private double delta=1;

    public SimpleFakeDataGenerator(double baseValue, double delta) {
        this.baseValue=baseValue;
        this.delta=delta;
    }

    public double getBaseValue() {
        return baseValue;
    }

    public void setBaseValue(double baseValue) {
        this.baseValue = baseValue;
    }

    public double getDelta() {
        return delta;
    }

    public void setDelta(double delta) {
        this.delta = delta;
    }
    
    
    
    @Override
    public SignalData generateData() {
        return new SignalData(System.currentTimeMillis(), baseValue+(Math.random()-0.5)*delta);
    }
    
}
