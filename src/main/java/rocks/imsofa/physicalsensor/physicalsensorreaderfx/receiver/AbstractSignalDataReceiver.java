/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.receiver;

import java.util.List;
import java.util.Vector;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalData;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalDataListener;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.exceptions.DataTypeMismatchException;

/**
 *
 * @author lendle
 */
public abstract class AbstractSignalDataReceiver implements SignalDataReceiver{
    protected String id=null;
    protected List<SignalDataListener> signalDataListeners=new Vector<>();
    private Class type=Number.class;

    public AbstractSignalDataReceiver(Class type) {
        this.type=type;
    }

    public AbstractSignalDataReceiver() {
    }
    
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Class getType() {
        return type;
    }

    public void setType(Class type) {
        this.type = type;
    }
    
    @Override
    public synchronized  void addDataListener(SignalDataListener signalDataListener) {
        signalDataListeners.add(signalDataListener);
    }

    @Override
    public synchronized void removeDataListener(SignalDataListener signalDataListener){
        signalDataListeners.remove(signalDataListener);
    }
    
    public synchronized void removeAllDataListeners(){
        signalDataListeners.clear();
    }
    
    protected synchronized void fireEvent(SignalData signalData) throws DataTypeMismatchException{
        if(!(type.isAssignableFrom(signalData.getValue().getClass()))){
            throw new DataTypeMismatchException(type, signalData.getValue().getClass());
        }
        for(SignalDataListener signalDataListener : this.signalDataListeners){
            signalDataListener.newData(this, signalData);
        }
    }
}
