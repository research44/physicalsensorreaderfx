/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.receiver;

import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalData;

/**
 *
 * @author lendle
 */
public interface FakeDataGenerator {
    public SignalData generateData();
}
