/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx;

import com.google.gson.Gson;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.apache.commons.io.FileUtils;
import rocks.imsofa.net.simplesocket.DefaultSimpleSocketImpl;
import rocks.imsofa.net.simplesocket.SimpleSocket;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.IPUtil;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.SerializationUtil;

/**
 * FXML Controller class
 *
 * @author lendle
 */
public class NewProjectDialogController {

    private File selectedDirectory = null;
    private OpenMode openMode = OpenMode.NEW;
    private SimpleSocket ss = null;
    private ControlThread controlThread = null;
    private PrimaryController controller = null;

    @FXML
    private Button buttonOk;
    @FXML
    private TextField textDirectory;

    @FXML
    public void initialize() {
        try {
            selectedDirectory = SerializationUtil.createTempDirectory();
            textDirectory.setText(selectedDirectory.getCanonicalPath());
        } catch (IOException ex) {
            Logger.getLogger(NewProjectDialogController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    void onChooseButtonAction(ActionEvent event) {
        DirectoryChooser chooser = new DirectoryChooser();
        if (selectedDirectory != null && selectedDirectory.exists()) {
            chooser.setInitialDirectory(selectedDirectory);
        }
        selectedDirectory = chooser.showDialog(buttonOk.getScene().getWindow());
        if (selectedDirectory != null) {
            try {
                textDirectory.setText(selectedDirectory.getCanonicalPath());
            } catch (IOException ex) {
                Logger.getLogger(NewProjectDialogController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @FXML
    void onOkButtonAction(ActionEvent event) {
        this.okButtonAction();
    }
    
    public void okButtonAction(){
        Thread workerThread = new Thread() {
            public void run() {
                openMode = OpenMode.NEW;

                Platform.runLater(() -> {
                    try {
                        Stage stage = openPrimaryWorkingDialog();
                        stage.showAndWait();
                    } catch (IOException ex) {
                        Logger.getLogger(NewProjectDialogController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });

            }
        };
        workerThread.start();
    }

    protected Stage openPrimaryWorkingDialog() throws IOException {
        //((Stage)buttonOk.getScene().getWindow()).close();
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("primary.fxml"));
        Parent root = fxmlLoader.load();
        controller = fxmlLoader.getController();
        controller.setProjectRootDirectory(this.getSelectedDirectory());
        controller.init();

        Scene scene = new Scene((Parent) root, 1000, 700);
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(scene);

        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent t) {
                ((PrimaryController) fxmlLoader.getController()).shutdown();
                if (ss != null) {
                    ss.stopReceivingMessage();
                    ss = null;
                }

            }
        });

//        controlThread = new ControlThread(controller);
//        controlThread.start();
        stage.setTitle("Main");
        return stage;
    }

    public File getSelectedDirectory() {
        return selectedDirectory;
    }

    public OpenMode getOpenMode() {
        return openMode;
    }

    @FXML
    void onLoadButtonAction(ActionEvent event) {
        Thread workerThread = new Thread() {
            public void run() {

                openMode = OpenMode.LOAD;

                Platform.runLater(() -> {
                    try {
                        Stage stage = openPrimaryWorkingDialog();
                        controller.load(getSelectedDirectory());
                        stage.showAndWait();
                    } catch (IOException ex) {
                        Logger.getLogger(NewProjectDialogController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });

            }
        };
        workerThread.start();

    }

    private class ControlThread extends Thread {

        private boolean running = true;
        private PrimaryController controller = null;

        public ControlThread(PrimaryController controller) {
            this.controller = controller;
            this.setDaemon(true);
        }

        public void shutdown() {
            running = false;
            this.interrupt();
        }

        public void run() {
            try {
                String config = FileUtils.readFileToString(new File("config.json"), "utf-8");
                Gson gson = new Gson();
                Map configMap = gson.fromJson(config, Map.class);
                Thread.sleep(30000);
                ss = new DefaultSimpleSocketImpl(10000);
//                System.out.println("new");
                ss.addMessageCallback(controller);
                ss.startReceivingMessage();
                while (running) {
                    try {
                        ss.send("" + configMap.get("controlpanel.ip"), Integer.valueOf("" + configMap.get("controlpanel.port")), IPUtil.getIntranetIP());
                        FileUtils.write(new File("out.log"), new Date().toString() + ":" + "" + configMap.get("controlpanel.ip") + ":" + Integer.valueOf("" + configMap.get("controlpanel.port")) + ":" + IPUtil.getIntranetIP(), "utf-8", true);
                        break;
                    } catch (Exception ex) {
                        //Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
                        try {
                            FileUtils.write(new File("out.log"), ex.getMessage(), "utf-8", true);
                        } catch (IOException ex1) {
                            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex1);
                        }
                        Thread.sleep(10000);
                    }
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(NewProjectDialogController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
