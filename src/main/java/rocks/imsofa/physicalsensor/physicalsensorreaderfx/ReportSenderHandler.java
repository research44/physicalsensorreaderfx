/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.auth.http.HttpCredentialsAdapter;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.TextInputDialog;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import static jdk.internal.org.jline.utils.Colors.h;
import net.lingala.zip4j.ZipFile;
import org.apache.commons.io.FileUtils;
import static org.renjin.nmath.signrank.w;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.SerializationUtil;

/**
 *
 * @author lendle
 */
public class ReportSenderHandler {

    private String title = null, headerText = null, contentText = null;
    private static final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();
    private static final List<String> SCOPES
            = List.of(DriveScopes.DRIVE_FILE, DriveScopes.DRIVE);
    private static final String TOKENS_DIRECTORY_PATH = "tokens";

    public ReportSenderHandler() {
        this.setTitle("寄送報告");
        this.setHeaderText("若要寄送報告，請輸入 Email");
        this.setContentText("Email:");
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHeaderText() {
        return headerText;
    }

    public void setHeaderText(String headerText) {
        this.headerText = headerText;
    }

    public String getContentText() {
        return contentText;
    }

    public void setContentText(String contentText) {
        this.contentText = contentText;
    }

    private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT)
            throws IOException {
        // Load client secrets.
        InputStream in = PrimaryController.class.getResourceAsStream("credentials.json");
        if (in == null) {
            throw new FileNotFoundException("Resource not found ");
        }
        GoogleClientSecrets clientSecrets
                = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
        Credential credential = new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
        //returns an authorized Credential object.
        return credential;
    }

    private String uploadToGoogleDrive(File reportFile) throws Exception {
        GoogleCredentials credentials = GoogleCredentials.getApplicationDefault()
                .createScoped(Arrays.asList(DriveScopes.DRIVE_FILE));
        HttpRequestInitializer requestInitializer = new HttpCredentialsAdapter(
                credentials);

        // Build a new authorized API client service.
        Drive service = new Drive.Builder(new NetHttpTransport(),
                GsonFactory.getDefaultInstance(),
                requestInitializer)
                .setApplicationName("Drive samples")
                .build();

        // Upload file photo.jpg on drive.
        com.google.api.services.drive.model.File fileMetadata = new com.google.api.services.drive.model.File();
        fileMetadata.setName(reportFile.getName());
        fileMetadata.setParents(Collections.singletonList("1XLwxzf1Z9eDHj-hdOnfOZV2SqgW8UTOr"));
        // File's content.
        java.io.File filePath = reportFile;
        // Specify media type and file-path for file.
        FileContent mediaContent = new FileContent("text/html", filePath);
        try {
            com.google.api.services.drive.model.File file = service.files().create(fileMetadata, mediaContent)
                    .setFields("webViewLink")
                    .execute();
            System.out.println("File ID: " + file.getId() + ":" + file.getWebViewLink() + ":" + file.getWebContentLink());
            return file.getWebViewLink();
        } catch (GoogleJsonResponseException e) {
            // TODO(developer) - handle error appropriately
            System.err.println("Unable to upload file: " + e.getDetails());
            throw e;
        }
    }

    public void execute(File reportFolder) {
//        TextInputDialog dialog = new TextInputDialog("Email");
//
//        dialog.setTitle(this.title);
//        dialog.setHeaderText(this.headerText);
//        dialog.setContentText(contentText);
//
//        Optional<String> result1 = dialog.showAndWait();
        File[] tempZipOutputFolders = new File[1];
        
            Thread emailThread = new Thread() {
                public void run() {
                    try {
                        tempZipOutputFolders[0] = SerializationUtil.createTempDirectory();
                        File tempZipOutputFolder = tempZipOutputFolders[0];
                        File tempZip = new File(tempZipOutputFolder, "report" + System.currentTimeMillis() + ".zip");
                        File outputQRCode=new File(tempZipOutputFolder, "report" + System.currentTimeMillis() + ".png");
                        new ZipFile(tempZip.getCanonicalPath()).addFolder(reportFolder);
                        String googleLink = uploadToGoogleDrive(tempZip);
//                        final Properties prop = new Properties();
//                        prop.put("mail.smtp.username", "lendle.tseng.archive@gmail.com");
//                        prop.put("mail.smtp.password", "uumgaqpqnqekkjgq");
//                        prop.put("mail.smtp.host", "smtp.gmail.com");
//                        prop.put("mail.smtp.port", "587");
//                        prop.put("mail.smtp.auth", "true");
//                        prop.put("mail.smtp.starttls.enable", "true"); // TLS
//                        Session mailSession = Session.getInstance(prop, new javax.mail.Authenticator() {
//                            @Override
//                            protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
//                                return new PasswordAuthentication(prop.getProperty("mail.smtp.username"),
//                                        prop.getProperty("mail.smtp.password"));
//                            }
//
//                        });
//                        Message message = new MimeMessage(mailSession);
//                        message.setFrom(new InternetAddress("lendle.tseng@gmail.com"));
//                        message.setSubject("人格特質分析報告");
//                        BodyPart mimeBody = new MimeBodyPart();
//                        String googleLink = uploadToGoogleDrive(tempZip);
//                        mimeBody.setContent("附檔爲人格特質分析報告，僅供參考: <a href='" + googleLink + "'>Download</a>", "text/html;charset=utf-8");
//                        /* Step 2: Create MimeMultipart and  wrap the mimebody to it */
//                        Multipart multiPart = new MimeMultipart();
//                        multiPart.addBodyPart(mimeBody);
//
////                        MimeBodyPart attachmentBodyPart = new MimeBodyPart();
////                        attachmentBodyPart.attachFile(tempZip);
////                        multiPart.addBodyPart(attachmentBodyPart);
//                        /* Step 3: set the multipart content to Message in caller method*/
//                        message.setContent(multiPart);
//                        InternetAddress[] toEmailAddresses
//                                = InternetAddress.parse(email);
//                        message.setRecipients(Message.RecipientType.TO, toEmailAddresses);
                        //Transport.send(message);

                        BitMatrix matrix = new MultiFormatWriter().encode(new String(googleLink.getBytes("utf-8"), "utf-8"), BarcodeFormat.QR_CODE, 200, 200);
                        MatrixToImageWriter.writeToPath(matrix, "png", outputQRCode.toPath());
//                        App.getApplication().getHostServices().showDocument(outputQRCode.toURI().toString());
                        Desktop.getDesktop().browse(outputQRCode.toURI());
                        
//                        Platform.runLater(new Runnable() {
//                            @Override
//                            public void run() {
//                                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
//                                alert.setContentText("Email 已寄出");
//                                alert.showAndWait();
//                            }
//
//                        });
                    } catch (Exception ex) {
                        Logger.getLogger(ReportSenderHandler.class.getName()).log(Level.SEVERE, null, ex);
                    } finally {
                        try {
//                            FileUtils.deleteDirectory(tempZipOutputFolders[0]);
                        } catch (Exception ex) {
                            Logger.getLogger(ReportSenderHandler.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            };
            emailThread.start();

    }
}
