package rocks.imsofa.physicalsensor.physicalsensorreaderfx;

import java.io.File;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaPlayer.Status;
import javafx.scene.media.MediaView;
import javafx.util.Duration;

public class MediaAppController {

    @FXML
    private MediaView mediaView;

    @FXML
    private Button bt_play;

    @FXML
    private Button bt_stop;

    @FXML
    private AnchorPane root;

    @FXML
    public void initialize() {
        try {
            mediaView.fitWidthProperty().bind(root.widthProperty());
            Media media = new Media(new File("file_example_MP4_480_1_5MG.mp4").toURI().toURL().toString());
            mediaView.setMediaPlayer(new MediaPlayer(media));
            mediaView.getMediaPlayer().currentTimeProperty().addListener(new ChangeListener<Duration>() {

                @Override
                public void changed(ObservableValue<? extends Duration> observable, Duration oldValue, Duration newValue) {
                    System.out.println(newValue);
                }
            });
            mediaView.getMediaPlayer().play();
        } catch (MalformedURLException ex) {
            Logger.getLogger(MediaAppController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    void onPlay(ActionEvent event) {
        mediaView.getMediaPlayer().play();
    }

    @FXML
    void onStop(ActionEvent event) {
        mediaView.getMediaPlayer().pause();
    }

}
