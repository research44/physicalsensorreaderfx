/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx;

import java.lang.reflect.Constructor;

/**
 *
 * @author lendle
 */
public class JepJavaClassInstanceFactory {
    public static Object newInstance(String className, String id) throws Exception{
        return Class.forName(className).getConstructor(new Class[]{String.class}).newInstance(id);
    }
}
