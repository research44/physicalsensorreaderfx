package rocks.imsofa.physicalsensor.physicalsensorreaderfx;

import java.io.File;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.stage.WindowEvent;
import org.apache.commons.io.FileUtils;
import rocks.imsofa.net.simplesocket.SimpleSocket;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;
    private static Application application=null;
    private SimpleSocket ss = null;
    private PrimaryController controller = null;
    
    public static Application getApplication(){
        return application;
    }

    @Override
    public void start(Stage stage) throws IOException {
        Parameters parameters=getParameters();
        String skipDialogString=parameters.getNamed().get("skipDialog");
        boolean skipDialog=(skipDialogString==null)?false:(Boolean.valueOf(skipDialogString));
        application=this;
        FXMLLoader newProjectDialogLoader = new FXMLLoader(App.class.getResource("newProjectDialog.fxml"));
        Parent newProjectRoot = newProjectDialogLoader.load();
        NewProjectDialogController newProjectDialogController = newProjectDialogLoader.getController();

        Scene scene = new Scene(newProjectRoot, 438, 219);
        Stage dialogStage = stage;
        stage.setTitle("PhysicalSensorReaderFX");
        dialogStage.setScene(scene);
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                //delete temp files
                File homeDirectory = new File(new File(System.getProperty("user.home")), ".physicalsensorreader");
                File tempDirectory = new File(homeDirectory, ".tmp");
                for (File file : tempDirectory.listFiles()) {
                    System.out.println("deleting " + file);
                    try {
                        FileUtils.deleteDirectory(file);
                    } catch (IOException ex) {
                        Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
        if(skipDialog){
            Platform.runLater(new Runnable(){
                public void run(){
                    newProjectDialogController.okButtonAction();
                }
            });
        }
        stage.show();
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
