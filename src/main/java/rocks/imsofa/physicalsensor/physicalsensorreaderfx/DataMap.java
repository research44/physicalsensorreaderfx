/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import rocks.imsofa.pagedrecordlist.PagedRecordCollectionList;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.SerializationUtil;

/**
 *
 * @author lendle
 */
public class DataMap implements Cloneable{
    /**
     * receiver id=>list
     */
    private Map<String, PagedRecordCollectionList> dataMap = new HashMap<>();
    
    public List<String> getDataMapKeys(){
        return new ArrayList<>(dataMap.keySet());
    }
    
    public PagedRecordCollectionList getPagedRecordCollectionList(String key){
        return this.dataMap.get(key);
    }
    
    public void setPageRecordCollectionList(String key, PagedRecordCollectionList list){
        this.dataMap.put(key, list);
    }
    
    public PagedRecordCollectionList getFrequenciesPagedRecordCollectionList(){
        return this.getPagedRecordCollectionList("frequencies");
    }
    
    public void setFrequenciesPageRecordCollectionList(PagedRecordCollectionList list){
        this.dataMap.put("frequencies", list);
    }
    
    public PagedRecordCollectionList getVolumesPagedRecordCollectionList(){
        return this.getPagedRecordCollectionList("volumes");
    }
    
    public void setVolumesPageRecordCollectionList(PagedRecordCollectionList list){
        this.dataMap.put("volumes", list);
    }
    
    public PagedRecordCollectionList getHrPagedRecordCollectionList(){
        return this.getPagedRecordCollectionList("hr");
    }
    
    public void setHrPageRecordCollectionList(PagedRecordCollectionList list){
        this.dataMap.put("hr", list);
    }
    
    public PagedRecordCollectionList getGSRPagedRecordCollectionList(){
        return this.getPagedRecordCollectionList("test3");
    }
    
    public void setGSRPageRecordCollectionList(PagedRecordCollectionList list){
        this.dataMap.put("test3", list);
    }
    
    public void clear(){
        this.dataMap.clear();
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        DataMap map2=new DataMap();
        map2.dataMap=new HashMap<>(this.dataMap);
        return map2;
    }
    
    public void shutdown() throws Exception{
        for (PagedRecordCollectionList list : dataMap.values()) {
            list.save();
        }
    }
    
    public void save(File projectRootDirectory) throws Exception {
        for (String id : dataMap.keySet()) {
            PagedRecordCollectionList list = dataMap.get(id);
            SerializationUtil.toCSV(new File(projectRootDirectory, id + ".csv"), list);
        }
    }
    
    public Map<String, PagedRecordCollectionList> toHashMap(){
        return new HashMap<>(this.dataMap);
    }
}
