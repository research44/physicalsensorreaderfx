package rocks.imsofa.physicalsensor.physicalsensorreaderfx;

import java.io.File;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import rocks.imsofa.net.simplesocket.MessageCallback;
import rocks.imsofa.pagedrecordlist.RecordEntry;
import rocks.imsofa.personality_assess.Output;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.receiver.SignalDataReceiver;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.AssessUtil;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.SerializationUtil;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.ThreadUtil;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.ClientData;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.PhysicalSensorReaderServer;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.RemoteSession;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.RemoteSessions;
import rocks.imsofa.rocksfx.table.DefaultTableCellRenderer;
import rocks.imsofa.rocksfx.table.TableColumnEx;

public class PrimaryController implements MessageCallback {

    @FXML
    private VBox container = null;
    @FXML
    private VBox simpleViewContainer;
//    @FXML
//    private ProgressBar progressBarFrequencies;
//
//    @FXML
//    private ProgressBar progressVolumes;
//
//    @FXML
//    private ProgressBar progressBarGSR;
//
//    @FXML
//    private ProgressBar progressBarHR;

    @FXML
    private Button buttonStart;

    @FXML
    private Button buttonStop;

    @FXML
    private Button buttonSave;

    @FXML
    private Button buttonAnalyze;

    @FXML
    private CheckBox checkboxUseTestData;

    @FXML
    private Tab tab_result;

    @FXML
    private Label label_ex_low;

    @FXML
    private Label label_ex_high;

    @FXML
    private Label label_co_low;

    @FXML
    private Label label_co_high;

    @FXML
    private Label label_ag_low;

    @FXML
    private Label label_ag_high;

    @FXML
    private Label label_es_low;

    @FXML
    private Label label_es_high;

    @FXML
    private Label label_op_low;

    @FXML
    private Label label_op_high;
    @FXML
    private TabPane tabPane;
    @FXML
    private ProgressIndicator progressIndicator;

    @FXML
    private TableView<ClientData> tableClients;

    //private SignalDataLineChart frequenciesChart = null, volumesChart = null, hrChart = null, gsrChart = null;
    private File projectRootDirectory = null;

    private double MAX_FREQUENCIES = 40, MAX_VOLUMES = 100, MAX_GSR = 600, MAX_HR = 100;
    private Output assessResult = null;

    @FXML
    private Label labelTextGSR;

    @FXML
    private Label labelTextHR;
//    private PhysicalSensorReaderModel physicalSensorReaderModel = null;
//    /**
//     * receiver id=>list
//     */
//    private DataMap dataMap = new DataMap();
//    private Session session = null;
    private PhysicalSensorReaderServer physicalSensorReaderServer = null;
    private ObservableList<String> clientIds = FXCollections.observableArrayList();
    private ObservableList<ClientData> clientDatas = FXCollections.observableArrayList();

    public File getProjectRootDirectory() {
        return projectRootDirectory;
    }

    public void setProjectRootDirectory(File projectRootDirectory) {
        this.projectRootDirectory = projectRootDirectory;
    }

    public void shutdown() {
        try {
            physicalSensorReaderServer.stop();
            for (Session session : physicalSensorReaderServer.getRemoteSessions().getSessions()) {
                session.shutdown();
            }
        } catch (Exception ex) {
            Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void init() {
        try {
            HBox group1 = new HBox();

//            frequenciesChart = new SignalDataLineChart();
//            frequenciesChart.setTitle("Frequencies");
//            frequenciesChart.setPrefSize(400, 250);
//            group1.getChildren().add(frequenciesChart);
//            volumesChart = new SignalDataLineChart();
//            volumesChart.setTitle("Volumes");
//            group1.getChildren().add(volumesChart);
//            volumesChart.setPrefSize(400, 250);
//            this.container.getChildren().add(group1);

            /*HBox group2 = new HBox();

            hrChart = new SignalDataLineChart();
            hrChart.setTitle("Heart Rate");
            hrChart.setLegendVisible(false);
            hrChart.setCreateSymbols(false);
            hrChart.getXAxis().setLabel("秒");
            hrChart.setPrefSize(400, 250);
            group2.getChildren().add(hrChart);

            gsrChart = new SignalDataLineChart();
            gsrChart.setTitle("GSR");
            gsrChart.setLegendVisible(false);
            gsrChart.setCreateSymbols(false);
            gsrChart.getXAxis().setLabel("秒");
            group2.getChildren().add(gsrChart);
            gsrChart.setPrefSize(400, 250);
            this.container.getChildren().add(group2);*/
//            session = new Session();
//            session.init(projectRootDirectory);
//            this.setSession(session);
            //initialize server
            TableColumnEx<ClientData, String> idColumn = new TableColumnEx<>("Id");
            TableColumnEx<ClientData, Double> gsrColumn = new TableColumnEx<>("GSR");
            TableColumnEx<ClientData, Double> hrColumn = new TableColumnEx<>("HR");
            idColumn.setTableCellRenderer(new DefaultTableCellRenderer<ClientData, String>("id"));
            gsrColumn.setTableCellRenderer(new DefaultTableCellRenderer<ClientData, Double>("gsr"));
            hrColumn.setTableCellRenderer(new DefaultTableCellRenderer<ClientData, Double>("hr"));
            idColumn.setMinWidth(130);
            tableClients.getColumns().clear();
            tableClients.getColumns().addAll(idColumn, gsrColumn, hrColumn);
            tableClients.setItems(clientDatas);
            tableClients.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<ClientData>(){
                @Override
                public void changed(ObservableValue<? extends ClientData> observable, ClientData oldValue, ClientData newValue) {
                    labelTextGSR.setText("-");
                    labelTextHR.setText("-");
                }
            });
            physicalSensorReaderServer = new PhysicalSensorReaderServer(projectRootDirectory, 8080);
            physicalSensorReaderServer.getRemoteSessions().addRemoteSessionListener(new RemoteSessions.RemoteSessionListener() {
                @Override
                public void clientRegistered(RemoteSession clientSession) {
                    synchronized (PrimaryController.this) {
                        boolean exists = false;
                        for (ClientData clientData : clientDatas) {
                            if (clientData.getId().equals(clientSession.getId())) {
                                exists = true;
                                break;
                            }
                        }
                        if (!exists) {
                            Platform.runLater(() -> {
                                ClientData clientData = new ClientData();
                                clientData.setId(clientSession.getId());
                                clientDatas.add(clientData);
                                clientData.setGsr(-1);
                            });
                        }
                        /*if (clientIds.contains(clientSession.getId())) {
                            return;
                        }
//                        try {
//                            clientSession.start();
//                        } catch (Exception ex) {
//                            Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
//                        }
                        Platform.runLater(() -> {
                            clientIds.add(clientSession.getId());
                        });*/
 /*if (session != null) {
                            initSession(clientSession);
                        }*/
                        setupSession(clientSession);
                    }

                }
            });

            physicalSensorReaderServer.start();
            start();
        } catch (Exception ex) {
            Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected void setupSession(Session session) {
        assessResult = null;
        /* if (this.session != null) {
            //clean previous session
            //frequenciesChart.clear();
//            volumesChart.clear();
//            hrChart.clear();
//            gsrChart.clear();
            session.removeAllDataListeners();
        }
        this.session = session;*/
        //session.addDataListener(SignalDataType.TYPE_FREQUENCY, new DefaultSignalDataListener("frequencies", progressBarFrequencies, MAX_FREQUENCIES));
        //frequenciesChart.setSignalDataReceiver(session.getSignalDataReceiver(SignalDataType.TYPE_FREQUENCY));
        //session.addDataListener(SignalDataType.TYPE_VOLUME, new DefaultSignalDataListener("volumes", progressVolumes, MAX_VOLUMES));
        //volumesChart.setSignalDataReceiver(session.getSignalDataReceiver(SignalDataType.TYPE_VOLUME));
        session.addDataListener(SignalDataType.TYPE_HR, new DefaultSignalDataListener((RemoteSession) session, "hr", labelTextHR));
        //hrChart.setSignalDataReceiver(session.getSignalDataReceiver(SignalDataType.TYPE_HR));
        session.addDataListener(SignalDataType.TYPE_GSR, new DefaultSignalDataListener((RemoteSession) session, "test3", labelTextGSR));
        try {
            //gsrChart.setSignalDataReceiver(session.getSignalDataReceiver(SignalDataType.TYPE_GSR));
            session.start();
        } catch (Exception ex) {
            Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    void onStartButtonAction(ActionEvent event) {
        start();
    }

    protected void start() {
        assessResult = null;
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                buttonStart.setDisable(true);
                buttonStop.setDisable(false);
                buttonSave.setDisable(true);
                buttonAnalyze.setDisable(true);
            }
        });

        try {
            for (Session session : physicalSensorReaderServer.getRemoteSessions().getSessions()) {
                session.start();
            }
        } catch (Exception ex) {
            Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    void onStopButtonAction(ActionEvent event) {
        stop();
    }

    protected void stop() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                buttonStart.setDisable(false);
                buttonStop.setDisable(true);
                buttonSave.setDisable(false);
                buttonAnalyze.setDisable(false);
            }
        });

        try {
            for (Session session : physicalSensorReaderServer.getRemoteSessions().getSessions()) {
                session.stop();
            }
        } catch (Exception ex) {
            Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    void onSaveButtonAction(ActionEvent event) {
        save();
    }

    protected void save() {
        try {
            for (Session session : physicalSensorReaderServer.getRemoteSessions().getSessions()) {
                session.save();
            }
        } catch (Exception ex) {
            Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Alert a = new Alert(Alert.AlertType.CONFIRMATION, "Saved!");
                a.showAndWait();
            }
        });

    }

    @FXML
    void onShutdownButtonAction(ActionEvent event) {
        for (Session session : physicalSensorReaderServer.getRemoteSessions().getSessions()) {
            try {
                ThreadUtil.submit(() -> {
                    try {
                        physicalSensorReaderServer.sendShutdownMessage(((RemoteSession) session).getId());
                    } catch (Exception ex) {
                        Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
            } catch (Exception ex) {
                Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void onMessage(String string, int i, String string1) {
//        if ("start".equals(string1)) {
//            this.start();
//        } else if ("stop".equals(string1)) {
//            this.stop();
//        } else if ("save".equals(string1)) {
//            this.save();
//        } else if ("reboot".equals(string1)) {
//            ProcessBuilder pb = new ProcessBuilder();
//            pb.command("sudo", "reboot");
//            try {
//                pb.start();
//            } catch (IOException ex) {
//                Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        } else if ("shutdown".equals(string1)) {
//            ProcessBuilder pb = new ProcessBuilder();
//            pb.command("sudo", "shutdown", "now");
//            try {
//                pb.start();
//            } catch (IOException ex) {
//                Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
    }

    @FXML
    void onAnalyzeButtonAction(ActionEvent event) {
        try {
            tabPane.getSelectionModel().select(tab_result);
            progressIndicator.setVisible(true);
            if (!verifyBeforeAssess()) {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setContentText("我們需要7分鐘的資料來進行分析，目前資料不足，可能影響準確度，是否確定進行？");
                Optional<ButtonType> result = alert.showAndWait();
                if (result.equals(ButtonType.NO)) {
                    return;
                }
            }
            Thread workerThread = new Thread() {
                public void run() {
                    try {
                        Output output = assess();
                        Platform.runLater(() -> {
                            if ((output.getExClassResults().get(0).getLevel().equals(rocks.imsofa.personality_assess.Level.HIGH))) {
                                label_ex_low.setStyle("-fx-background-color: white;-fx-border-color: black");
                                label_ex_high.setStyle("-fx-background-color: yellow;-fx-border-color: black");
                            } else {
                                label_ex_low.setStyle("-fx-background-color: yellow;-fx-border-color: black");
                                label_ex_high.setStyle("-fx-background-color: white;-fx-border-color: black");
                            }
                            if ((output.getCoClassResults().get(0).getLevel().equals(rocks.imsofa.personality_assess.Level.HIGH))) {
                                label_co_low.setStyle("-fx-background-color: white;-fx-border-color: black");
                                label_co_high.setStyle("-fx-background-color: yellow;-fx-border-color: black");
                            } else {
                                label_co_low.setStyle("-fx-background-color: yellow;-fx-border-color: black");
                                label_co_high.setStyle("-fx-background-color: white;-fx-border-color: black");
                            }
                            if ((output.getAgClassResults().get(0).getLevel().equals(rocks.imsofa.personality_assess.Level.HIGH))) {
                                label_ag_low.setStyle("-fx-background-color: white;-fx-border-color: black");
                                label_ag_high.setStyle("-fx-background-color: yellow;-fx-border-color: black");
                            } else {
                                label_ag_low.setStyle("-fx-background-color: yellow;-fx-border-color: black");
                                label_ag_high.setStyle("-fx-background-color: white;-fx-border-color: black");
                            }
                            if ((output.getEsClassResults().get(0).getLevel().equals(rocks.imsofa.personality_assess.Level.HIGH))) {
                                label_es_low.setStyle("-fx-background-color: white;-fx-border-color: black");
                                label_es_high.setStyle("-fx-background-color: yellow;-fx-border-color: black");
                            } else {
                                label_es_low.setStyle("-fx-background-color: yellow;-fx-border-color: black");
                                label_es_high.setStyle("-fx-background-color: white;-fx-border-color: black");
                            }
                            if ((output.getOpClassResults().get(0).getLevel().equals(rocks.imsofa.personality_assess.Level.HIGH))) {
                                label_op_low.setStyle("-fx-background-color: white;-fx-border-color: black");
                                label_op_high.setStyle("-fx-background-color: yellow;-fx-border-color: black");
                            } else {
                                label_op_low.setStyle("-fx-background-color: yellow;-fx-border-color: black");
                                label_op_high.setStyle("-fx-background-color: white;-fx-border-color: black");
                            }
                        });

                    } catch (Exception ex) {
                        Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
                    } finally {
                        progressIndicator.setVisible(false);
                    }
                }
            };
            workerThread.start();
        } catch (Exception ex) {
            Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //load from the given dir
    public void load(File dir) {
        if (dir != null) {
            //sub directories
            File[] subDirs = dir.listFiles();
            for (File subDir : subDirs) {
                try {
                    RemoteSession remoteSession = new RemoteSession();
                    remoteSession.setId(subDir.getName());
                    remoteSession.init(subDir);
                    remoteSession.load(dir);
                    physicalSensorReaderServer.getRemoteSessions().register(subDir.getName(), remoteSession);
//                    Platform.runLater(()->{
//                        clientIds.add(subDir.getName());
//                    });
                } //            session.load(dir);
                //            PagedRecordCollectionList hrList = session.getPagedRecordCollectionList(SignalDataType.TYPE_HR);
                //            hrChart.loadFromPagedRecordCollectionList(hrList);
                //            PagedRecordCollectionList gsrList = session.getPagedRecordCollectionList(SignalDataType.TYPE_GSR);
                //            gsrChart.loadFromPagedRecordCollectionList(gsrList);
                //            PagedRecordCollectionList volumesList = session.getPagedRecordCollectionList(SignalDataType.TYPE_VOLUME);
                //            volumesChart.loadFromPagedRecordCollectionList(volumesList);
                //            PagedRecordCollectionList frequenciesList = session.getPagedRecordCollectionList(SignalDataType.TYPE_FREQUENCY);
                //            frequenciesChart.loadFromPagedRecordCollectionList(frequenciesList);
                catch (Exception ex) {
                    Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        this.buttonAnalyze.setDisable(false);
    }

    protected RemoteSession getSelectedSession() {
        ClientData client = tableClients.getSelectionModel().getSelectedItem();
        if (client != null) {
            for (RemoteSession session : physicalSensorReaderServer.getRemoteSessions().getSessions()) {
                if (session.getId().equals(client.getId())) {
                    return session;
                }
            }
        }
        return null;

    }

    @FXML
    void onReportButtonAction(ActionEvent event) {
        try {
            DirectoryChooser directoryChooser = new DirectoryChooser();
            directoryChooser.setInitialDirectory(projectRootDirectory);
            directoryChooser.setTitle("Choose report export destination:");
            File file = directoryChooser.showDialog(label_ag_high.getScene().getWindow());
            if (this.assessResult == null && !verifyBeforeAssess()) {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setContentText("我們需要7分鐘的資料來進行分析，目前資料不足，可能影響準確度，是否確定進行？");
                Optional<ButtonType> result = alert.showAndWait();
                if (result.equals(ButtonType.NO)) {
                    return;
                }
            }

            if (file != null) {
                try {
                    progressIndicator.setVisible(true);
                    Thread workerThread = new Thread() {
                        public void run() {
                            try {
                                Output output = assess();
                                if (output == null) {
                                    return;
                                }
                                RemoteSession session = getSelectedSession();
                                if (session != null) {
                                    SerializationUtil.exportReport(file, session.getDataMap().toHashMap(), output);
                                }
                            } catch (Exception e) {
                            }
                            Platform.runLater(() -> {
                                progressIndicator.setVisible(false);
                                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                                alert.setContentText("Done! Open it?");
                                Optional<ButtonType> result = alert.showAndWait();
                                if (result.get().equals(ButtonType.OK)) {
                                    try {
                                        App.getApplication().getHostServices().showDocument(new File(file, "index.html").toURI().toString());

                                    } catch (Exception ex) {
                                        Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                }

                                ReportSenderHandler reh = new ReportSenderHandler();
                                reh.execute(file);
                            });
                        }
                    };
                    workerThread.setDaemon(true);
                    workerThread.start();
                } catch (Exception ex) {
                    Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    void onChartButtonAction(ActionEvent event) {
        try {
            File file=SerializationUtil.createTempDirectory();

            if (file != null) {
                try {
                    progressIndicator.setVisible(true);
                    Thread workerThread = new Thread() {
                        public void run() {
                            try {
                                RemoteSession session = getSelectedSession();
                                if (session != null) {
                                    SerializationUtil.exportReport(file, session.getDataMap().toHashMap(), null);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            Platform.runLater(() -> {
                                progressIndicator.setVisible(false);
                                App.getApplication().getHostServices().showDocument(new File(file, "index.html").toURI().toString());
                            });
                        }
                    };
                    workerThread.setDaemon(true);
                    workerThread.start();
                } catch (Exception ex) {
                    Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    protected boolean verifyBeforeAssess() throws Exception {
        RemoteSession session = getSelectedSession();
        if (session != null) {
            return AssessUtil.verify(session.getDataMap());
        } else {
            return false;
        }
    }

    protected Output assess() throws Exception {
        if (this.assessResult != null) {
            return this.assessResult;
        }
        RemoteSession session = getSelectedSession();
        if (session != null) {
            Output output = AssessUtil.assess(session.getDataMap(), checkboxUseTestData.isSelected());
            assessResult = output;
            return output;
        } else {
            return null;
        }
    }

    private class DefaultSignalDataListener implements SignalDataListener {

        private String recordListId = null;
        private Label dataHolder = null;
        private RemoteSession session = null;

        public DefaultSignalDataListener(RemoteSession session, String recordListId, Label dataHolder) {
            this.recordListId = recordListId;
            this.dataHolder = dataHolder;
            this.session = session;
        }

        @Override
        public void newData(SignalDataReceiver source, SignalData data) {
            try {
                RecordEntry entry = new RecordEntry();
                entry.setValue("timestamp", data.getTimestamp());
                entry.setValue("value", data.getValue());
//                dataMap.get(recordListId).add(entry);
                session.getDataMap().getPagedRecordCollectionList(recordListId).add(entry);
                Platform.runLater(() -> {
                    RemoteSession selectedSession=getSelectedSession();
                    if(selectedSession!=null && selectedSession.getId().equals(session.getId())){
                        dataHolder.setText(String.format("%7.2f", data.getValue()));
                    }
                    
//                    dataHolder.setText(""+data.getValue());
//                    progressBar.setProgress((Double) data.getValue() / maxValue);
                    for (ClientData clientData : clientDatas) {
                        if (clientData.getId().equals(session.getId())) {
                            if (recordListId.equals("hr")) {
                                clientData.setHr((Double) data.getValue());
                            } else {
                                clientData.setGsr((Double) data.getValue());
                            }
                        }
                    }
                    tableClients.refresh();
                });
            } catch (Exception ex) {
                Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    @FXML
    void onSwitchClient(ActionEvent event) {
        //RemoteSession session = physicalSensorReaderServer.getRemoteSessions().getRemoteSession(listClients.getSelectionModel().getSelectedItem());
        //this.setupSession(session);
    }

    @FXML
    void onBeepButtonAction(ActionEvent event) {
        RemoteSession selectedSession=getSelectedSession();
        for (Session session : physicalSensorReaderServer.getRemoteSessions().getSessions()) {
            RemoteSession remoteSession = (RemoteSession) session;
            if (remoteSession.getId().equals(selectedSession.getId()) == false) {
                continue;
            }
            try {
                ThreadUtil.submit(() -> {
                    try {
                        physicalSensorReaderServer.sendBeepMessage(((RemoteSession) session).getId());
                    } catch (Exception ex) {
                        Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
            } catch (Exception ex) {
                Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @FXML
    void onRenameButtonAction(ActionEvent event) {
        RemoteSession selectedSession=getSelectedSession();
        for (Session session : physicalSensorReaderServer.getRemoteSessions().getSessions()) {
            RemoteSession remoteSession = (RemoteSession) session;
            if (remoteSession.getId().equals(selectedSession.getId()) == false) {
                continue;
            }
            try {
                TextInputDialog renameDialog = new TextInputDialog();
                renameDialog.setTitle("Rename");
                renameDialog.setContentText("Enter the new name\r\n(effective after rebooting): ");
                renameDialog.setHeaderText("Rename " + remoteSession.getId());
                Optional<String> name = renameDialog.showAndWait();
                if (name.isPresent()) {
                    //listClients.getItems().remove(remoteSession.getId());
                    ThreadUtil.submit(() -> {
                        try {
                            physicalSensorReaderServer.sendRenameMessage(((RemoteSession) session).getId(), name.get());
                        } catch (Exception ex) {
                            Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    });
                }

            } catch (Exception ex) {
                Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    @FXML
    void onResetButtonAction(ActionEvent event) {
        RemoteSession selectedSession=getSelectedSession();
        for (Session session : physicalSensorReaderServer.getRemoteSessions().getSessions()) {
            RemoteSession remoteSession = (RemoteSession) session;
            if (remoteSession.getId().equals(selectedSession.getId()) == false) {
                continue;
            }else{
                try {
                    remoteSession.reset();
                } catch (Exception ex) {
                    Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
