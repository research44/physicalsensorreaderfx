/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import rocks.imsofa.pagedrecordlist.PagedRecordCollectionList;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.receiver.FakeDataGeneratorThread;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.receiver.InterruptSignalDataReceiver;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.receiver.JepSignalDataReceiver;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.receiver.SignalDataReceiver;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.receiver.SimpleFakeDataGenerator;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.sound.SoundAnalyzerThread;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.SerializationUtil;

/**
 *
 * @author lendle
 */
public class PhysicalSensorReaderModel {

    protected List<SignalDataReceiver> receivers = new ArrayList<>();
    private SoundAnalyzerThread soundAnalyzerThread = null;
    private int fakeBaseValue=(int)(Math.random()*100);
    /**
     * receiver id=>list
     */
    //DataMap dataMap = new DataMap();
    protected File projectRootDirectory = null;

    public PhysicalSensorReaderModel(File projectRootDirectory) throws Exception {
        this.projectRootDirectory = projectRootDirectory;
        soundAnalyzerThread = new SoundAnalyzerThread();
        //soundAnalyzerThread.start();
        initSignalDataReceivers();
    }
    
    protected PhysicalSensorReaderModel(){
    }
    /**
     * can override this method to implement ws branch for remote data sources
     * @throws Exception 
     */
    protected void initSignalDataReceivers() throws Exception{
        InterruptSignalDataReceiver frequencyReceiver = createFrequencySignalDataReceiver();
        frequencyReceiver.setId("frequencies");
        receivers.add(frequencyReceiver);

        InterruptSignalDataReceiver volumeReceiver = createVolumesSignalDataReceiver();
        volumeReceiver.setId("volumes");
        receivers.add(volumeReceiver);

        JepSignalDataReceiver proxy = (JepSignalDataReceiver) createHrSignalDataReceiver();
        proxy.setId("hr");
        
        receivers.add(proxy);
        JepSignalDataReceiver proxy2 = (JepSignalDataReceiver) createGSRSignalDataReceiver();
        proxy2.setId("test3");
        
        receivers.add(proxy2);
    }
    /**
     * can override this method to implement ws branch for remote data sources
     * @throws Exception 
     */
    protected InterruptSignalDataReceiver createFrequencySignalDataReceiver(){
        return new InterruptSignalDataReceiver(soundAnalyzerThread.getFrequencies(), soundAnalyzerThread);
    }
    /**
     * can override this method to implement ws branch for remote data sources
     * @throws Exception 
     */
    protected InterruptSignalDataReceiver createVolumesSignalDataReceiver(){
        return new InterruptSignalDataReceiver(soundAnalyzerThread.getVolumes(), soundAnalyzerThread);
    }
    /**
     * can override this method to implement ws branch for remote data sources
     * @throws Exception 
     */
    protected InterruptSignalDataReceiver createHrSignalDataReceiver(){
        return new JepSignalDataReceiver(new File("./physical_signal/hr2.py"), "hr");
    }
    /**
     * can override this method to implement ws branch for remote data sources
     * @throws Exception 
     */
    protected InterruptSignalDataReceiver createGSRSignalDataReceiver(){
        return new JepSignalDataReceiver(new File("./physical_signal/gsr.py"), "gsr");
    }
    
    public void start(boolean fakeMode) throws Exception {
        for (SignalDataReceiver receiver : receivers) {
            if (!fakeMode) {
                receiver.start();
            } else {
                InterruptSignalDataReceiver interruptSignalDataReceiver = (InterruptSignalDataReceiver) receiver;
                if (interruptSignalDataReceiver.getId().equals("frequencies")) {
                    interruptSignalDataReceiver.start(new FakeDataGeneratorThread(new SimpleFakeDataGenerator(30+fakeBaseValue, 10), 2000));
                } else if (interruptSignalDataReceiver.getId().equals("volumes")) {
                    interruptSignalDataReceiver.start(new FakeDataGeneratorThread(new SimpleFakeDataGenerator(80+fakeBaseValue, 20), 2000));
                } else if (interruptSignalDataReceiver.getId().equals("hr")) {
                    interruptSignalDataReceiver.start(new FakeDataGeneratorThread(new SimpleFakeDataGenerator(80+fakeBaseValue, 200), 2000));
                } else if (interruptSignalDataReceiver.getId().equals("test3")) {
                    interruptSignalDataReceiver.start(new FakeDataGeneratorThread(new SimpleFakeDataGenerator(400+fakeBaseValue, 500), 2000));
                }
            }
        }
    }

    public void start() throws Exception {
        this.start(false);
    }

    public void stop() throws Exception {
        for (SignalDataReceiver receiver : receivers) {
            receiver.stop();
        }
    }

    public List<SignalDataReceiver> getReceivers() {
        return new ArrayList<>(receivers);
    }

    public InterruptSignalDataReceiver getFrequencySignalDataReceiver() {
        return this.getSignalDataReceiver("frequencies");
    }

    public InterruptSignalDataReceiver getVolumesSignalDataReceiver() {
        return this.getSignalDataReceiver("volumes");
    }

    public InterruptSignalDataReceiver getHrSignalDataReceiver() {
        return this.getSignalDataReceiver("hr");
    }

    public InterruptSignalDataReceiver getGSRSignalDataReceiver() {
        return this.getSignalDataReceiver("test3");
    }

    private InterruptSignalDataReceiver getSignalDataReceiver(String id) {
        for (SignalDataReceiver signalDataReceiver : this.receivers) {
            if (signalDataReceiver.getId().equals(id)) {
                return (InterruptSignalDataReceiver) signalDataReceiver;
            }
        }
        return null;
    }
}
