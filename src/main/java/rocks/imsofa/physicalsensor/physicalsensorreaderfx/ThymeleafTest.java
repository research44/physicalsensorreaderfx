/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

/**
 *
 * @author lendle
 */
public class ThymeleafTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception{
        // TODO code application logic here
        TemplateEngine templateEngine = new TemplateEngine();
        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setTemplateMode("HTML");
        templateEngine.setTemplateResolver(templateResolver);
        Context context = new Context();
        context.setVariable("name", "World");
        Map reportData=new HashMap();
        List charts=new ArrayList();
        reportData.put("charts", charts);
        {
            Map hrChart=new HashMap();
            hrChart.put("name", "hr");
            hrChart.put("labels", new long[]{0, 5, 10, 15, 20, 25, 30});
            hrChart.put("values", new double[]{50, 60, 30, 20, 40, 50, 60});
            hrChart.put("color", "red");
            charts.add(hrChart);
        }
        
        {
            Map chart=new HashMap();
            chart.put("name", "gsr");
            chart.put("labels", new long[]{0, 5, 10, 15, 20, 25, 30});
            chart.put("values", new double[]{50, 60, 30, 20, 40, 50, 60});
            chart.put("color", "blue");
            charts.add(chart);
        }
        context.setVariable("reportData", reportData);
        StringWriter stringWriter = new StringWriter();
        templateResolver.setPrefix("template/");
        templateResolver.setSuffix(".html");
        templateResolver.setCharacterEncoding("utf-8");
        templateEngine.process("test", context, stringWriter);
        System.out.println(stringWriter.toString());
    }
    
}
