/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.multicast;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.NetworkInterface;
import java.util.logging.Level;
import java.util.logging.Logger;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.NetworkInterfaceUtil;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.PropertiesUtil;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.Constants;

/**
 *
 * @author lendle
 */
public class MulticastReceiver extends Thread {

    private String group = null;
    private int port = -1;
    private boolean running = true;
    private Callback callback=null;

    public MulticastReceiver(String group, int port, Callback callback) {
        this.group = group;
        this.port = port;
        this.callback=callback;
        this.setDaemon(true);
    }
    
    public MulticastReceiver(Callback callback) {
        this(Constants.MULTICAST_GROUP, Constants.MULTICAST_PORT, callback);
    }

    public void run() {
        MulticastSocket socket =null;
        try {
//            Enumeration<NetworkInterface> enumeration = NetworkInterface.getNetworkInterfaces();
//            while (enumeration.hasMoreElements()) {
//                NetworkInterface networkInterface = enumeration.nextElement();
//                System.out.println(networkInterface.getDisplayName());
//            }
            socket=new MulticastSocket(port);
            String niName=PropertiesUtil.getNetworkInterface();
            if(niName!=null){
                NetworkInterface ni=NetworkInterfaceUtil.getNetworkInterface(niName);
                Logger.getLogger(MulticastReceiver.class.getName()).info("use network interface: "+niName+":"+ni);
                socket.setNetworkInterface(ni);
            }else{
                Logger.getLogger(MulticastReceiver.class.getName()).info("use default network interface");
            }
            Logger.getLogger(MulticastSocket.class.getName()).info("send broadcast niName = "+niName+", groupIP="+group);
            InetAddress inetAddress = InetAddress.getByName(group);
            socket.joinGroup(inetAddress);
        } catch (IOException ex) {
            Logger.getLogger(MulticastReceiver.class.getName()).log(Level.SEVERE, null, ex);
        }
        while (running) {
            try {
                DatagramPacket packet;
                byte[] buf = new byte[1024];
                packet = new DatagramPacket(buf, buf.length);
                socket.receive(packet);
                String received = new String(packet.getData()).trim();
                callback.received(received);
            } catch (Exception e) {
                Logger.getLogger(this.getClass().getCanonicalName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }
    
    public void shutdown(){
        this.running=false;
        this.interrupt();
    }

    public interface Callback {

        public void received(String message);
    }
}
