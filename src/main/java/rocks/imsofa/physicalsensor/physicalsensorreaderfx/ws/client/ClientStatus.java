/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Enum.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.client;

/**
 *
 * @author lendle
 */
public enum ClientStatus {
    STANDALONE,
    AWAITING_SERVER,
    BOUND_SERVER
}
