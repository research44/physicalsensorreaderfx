/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.handlers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.GlobalContext;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.client.BeepCommand;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.client.ClientGUICommandEventListener;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.client.RenameCommand;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.CommandMessage;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.Message;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.servlets.MessageResponse;

/**
 * received a message from external data sources and store in local data sink
 *
 * @author lendle
 */
public class CommandMessageHandler implements MessageHandler {

    private GlobalContext globalContext = null;
    private List<ClientGUICommandEventListener> clientGUICommandEventListeners = new ArrayList<>();

    public CommandMessageHandler(GlobalContext globalContext) {
        this.globalContext = globalContext;
    }

    public void addClientGUICommandEventListener(ClientGUICommandEventListener l) {
        this.clientGUICommandEventListeners.add(l);
    }

    @Override
    public boolean canHandle(Message message) {
        return message instanceof CommandMessage;
    }

    @Override
    public MessageResponse process(Message message) {
        CommandMessage commandMessage = (CommandMessage) message;
        String command = commandMessage.getCommand();
        Logger.getLogger(this.getClass().getName()).info("received command: "+command);
        if ("reboot".equals(command)) {
            ProcessBuilder pb = new ProcessBuilder();
            pb.command("sudo", "reboot");
            try {
                pb.start();
            } catch (IOException ex) {
                Logger.getLogger(CommandMessageHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if ("shutdown".equals(command)) {
            ProcessBuilder pb = new ProcessBuilder();
            pb.command("sudo", "shutdown", "now");
            try {
                pb.start();
            } catch (IOException ex) {
                Logger.getLogger(CommandMessageHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if ("beep".equals(command)) {
            BeepCommand beepCommand = new BeepCommand();
            for (ClientGUICommandEventListener l : this.clientGUICommandEventListeners) {
                l.commandReceived(beepCommand);
            }
        } else if ("rename".equals(command)) {
            RenameCommand renameCommand = new RenameCommand();
            renameCommand.setNewName(commandMessage.getArgs().get(0));
            for (ClientGUICommandEventListener l : this.clientGUICommandEventListeners) {
                l.commandReceived(renameCommand);
            }
        }else if ("kill".equals(command)) {
            Logger.getLogger(this.getClass().getName()).info("killing the application");
            System.exit(0);
        }
        return new MessageResponse();
    }

}
