/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.handlers;

import java.util.logging.Logger;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalData;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.RemoteSession;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.GlobalContext;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.Message;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.SignalDataMessage;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.servlets.MessageResponse;

/**
 * received a message from external data sources
 * and store in local data sink
 * @author lendle
 */
public class SignalDataMessageHandler implements MessageHandler {
    protected GlobalContext globalContext=null;

    public SignalDataMessageHandler(GlobalContext globalContext) {
        this.globalContext=globalContext;
    }
    
    
    @Override
    public boolean canHandle(Message message) {
        return message instanceof SignalDataMessage;
    }

    @Override
    public MessageResponse process(Message message) {
        //TODO: put signal into session DataMap
        SignalDataMessage signalDataMessage=(SignalDataMessage) message;
        RemoteSession remoteSession=globalContext.getRemoteSessions().getRemoteSession(signalDataMessage.getSenderId());
        SignalData signalData=new SignalData(signalDataMessage.getTimestamp(), signalDataMessage.getValue());
        switch(signalDataMessage.getSignalType()){
            case SignalDataMessage.TYPE_FREQUENCY:
                //remoteSession.getModel().getFrequencySignalDataReceiver().putSignalData(signalData);
                break;
            case SignalDataMessage.TYPE_VOLUME:
                //remoteSession.getModel().getVolumesSignalDataReceiver().putSignalData(signalData);
                break;
            case SignalDataMessage.TYPE_HR:
                remoteSession.getModel().getHrSignalDataReceiver().putSignalData(signalData);
                break;
            case SignalDataMessage.TYPE_GSR:
                remoteSession.getModel().getGSRSignalDataReceiver().putSignalData(signalData);
                break;
        }
//        Logger.getLogger(this.getClass().getName()).info("signal data: "+signalData+" is received");
        return new MessageResponse();
    }
    
}
