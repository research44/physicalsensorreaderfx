/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.client;

import java.util.ArrayList;
import java.util.List;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.Message;

/**
 *
 * @author lendle
 */
public class ClientMessageQueue {
    private List<Message> messages=new ArrayList<>();
    
    public synchronized void addMessage(Message message){
        messages.add(message);
    }
    
    public synchronized boolean isEmpty(){
        return messages.isEmpty();
    }
    
    public synchronized List<Message> fetchAll(){
        List<Message> cloned=new ArrayList<>(messages);
        messages.clear();
        return cloned;
    }
}
