/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.messagebus;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.IPUtil;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.ThreadUtil;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.multicast.MulticastReceiver;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.multicast.MulticastUtil;

/**
 *
 * @author lendle
 */
public abstract class AbstractJettyMessageBus implements MessageBus {

    private static final int COLOAD_PROCESS_INTERVAL = 3000;
    protected Server server = null;
    private MulticastReceiver multicastReceiver = null;
    //remote id => list of coloaded queued messages waiting to be sent
    private Map<String, List<QueuedMessage>> messageQueue = new HashMap<>();

    /**
     *
     * @param localId
     * @param port -1 for random port
     */
    public AbstractJettyMessageBus(int port) {
        server = new Server();
        ServerConnector connector = new ServerConnector(server);
        if (port != -1) {
            connector.setPort(port);
        }
        server.setConnectors(new Connector[]{connector});
        ServletContextHandler ctx = new ServletContextHandler();
        ctx.setContextPath("/");
        ctx.addServlet(new ServletHolder(new MessageHandlingServlet()), "/message");
        ctx.addServlet(new ServletHolder(new MultiMessageHandlingServlet()), "/multimessage");
        multicastReceiver = new MulticastReceiver(new MulticastReceiver.Callback() {
            @Override
            public void received(String message) {
                broadcastMessageReceived(message);
            }
        });
        multicastReceiver.start();
        server.setHandler(ctx);
        ColoadMessageSenderDaemon coloadMessageSenderDaemon = new ColoadMessageSenderDaemon();
        coloadMessageSenderDaemon.start();
    }

    public Server getServer() {
        return server;
    }

    @Override
    public String getLocalId() {
        return IPUtil.getIntranetIP() + "_" + getLocalPort();
    }

    public int getLocalPort() {
        return ((ServerConnector) this.server.getConnectors()[0]).getLocalPort();
    }

    public String getLocalIp() {
        return IPUtil.getIntranetIP();
    }

    @Override
    public void sendBroadcastMessage(String message) throws Exception {
        if (!checkSendingMessageFormat(true, message)) {
            throw new Exception("message not in correct format");
        }
        ThreadUtil.submit(() -> {
            MulticastUtil.multicast(message);
        });
    }

    @Override
    public synchronized void sendDirectMessage(String remoteId, boolean allowColoading, String message, MessageCallback messageCallback) throws Exception {
        if (!checkSendingMessageFormat(false, message)) {
            throw new Exception("message not in correct format");
        }
        if (remoteId == null) {
            return;
        }
        if (allowColoading) {
            List<QueuedMessage> queue = messageQueue.get(remoteId);
            if (queue == null) {
                queue = new ArrayList<>();
                messageQueue.put(remoteId, queue);
            }
            queue.add(new QueuedMessage(message, messageCallback));
        } else {
            ThreadUtil.submit(() -> {
//                System.out.println("submit message");
//                System.out.println("\t"+message);
                String endPoint = this.getEndpoint(remoteId, false);
                try {
                    String response = HttpRequest.post(endPoint).contentType("application/json", "utf-8").send(message).body();
//                    System.out.println("\tresponse="+response);
                    if (messageCallback != null) {
                        messageCallback.responseReceived(remoteId, allowColoading, response);
                    }
                } catch (Throwable e) {
                    //in case there is an error
                    messageCallback.failed(remoteId, allowColoading, e);
                }
            });

        }
    }

    @Override
    public void start() throws Exception {
        server.start();
    }

    @Override
    public void stop() throws Exception {
        server.stop();
        ThreadUtil.shutdownNow();
    }

    /**
     * build the endpoint for the given id
     *
     * @param remoteId
     * @return
     */
    protected abstract String getEndpoint(String remoteId, boolean coloadingMessages);

    /**
     * expect a utf-8 encoded string in post body and a sender parameter with
     * remoteId
     */
    class MessageHandlingServlet extends HttpServlet {

        protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            String str = IOUtils.toString(req.getInputStream(), "utf-8");
            String remoteId = req.getParameter("sender");
            String ret = AbstractJettyMessageBus.this.singleDirectMessageReceived(remoteId, str);
            IOUtils.write(ret, resp.getOutputStream(), "utf-8");
        }
    }

    /**
     * run in background to send non-immediate messages in a batch manner
     */
    class ColoadMessageSenderDaemon extends Thread {

        public ColoadMessageSenderDaemon() {
            this.setDaemon(true);
        }

        public void run() {
            while (true) {
                try {
                    Thread.sleep(COLOAD_PROCESS_INTERVAL);
                    Gson gson = new Gson();
                    synchronized (AbstractJettyMessageBus.this) {
                        for (String remoteId : messageQueue.keySet()) {
                            String endPoint = getEndpoint(remoteId, true);
                            if (endPoint == null) {
                                Logger.getLogger(AbstractJettyMessageBus.class.getName()).warning("endpoing " + remoteId + " skipped");
                                continue;
                            }
                            List<QueuedMessage> messages = new ArrayList<>(messageQueue.get(remoteId));
                            messageQueue.get(remoteId).clear();
                            List<String> messageContents = new ArrayList<>();
                            for (QueuedMessage queuedMessage : messages) {
                                messageContents.add(queuedMessage.getMessage());
                            }

                            //the response is a list of strings
                            String response = null;
                            try {
                                response = HttpRequest.post(endPoint).contentType("application/json", "utf-8").send(gson.toJson(messageContents)).body();
                                List<String> replies = gson.fromJson(response, List.class);
                                for (int i = 0; i < replies.size(); i++) {
                                    if (messages.get(i).callback != null) {
                                        messages.get(i).callback.responseReceived(remoteId, true, replies.get(i));
                                    }
                                }
                            } catch (Throwable e) {
                                //in case the message can not be delivered successfully
                                for (QueuedMessage queuedMessage : messages) {
                                    if(queuedMessage.callback!=null){
                                        queuedMessage.callback.failed(remoteId, true, e);
                                    }
                                }
                            }

                        }
                    }
                } catch (InterruptedException ex) {
                    Logger.getLogger(AbstractJettyMessageBus.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

    /**
     * the received message must be a json array of strings and a sender
     * parameter with remoteId
     */
    class MultiMessageHandlingServlet extends HttpServlet {

        protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            String str = IOUtils.toString(req.getInputStream(), "utf-8");
            String remoteId = req.getParameter("sender");
            Gson gson = new Gson();
            List<String> messages = gson.fromJson(str, List.class);
            List<String> ret = AbstractJettyMessageBus.this.coLoadedDirectMessagesReceived(remoteId, messages);
            IOUtils.write(gson.toJson(ret), resp.getOutputStream(), "utf-8");
        }
    }

    static class QueuedMessage {

        private String message = null;
        private MessageCallback callback = null;

        public QueuedMessage(String message, MessageCallback callback) {
            this.message = message;
            this.callback = callback;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public MessageCallback getCallback() {
            return callback;
        }

        public void setCallback(MessageCallback callback) {
            this.callback = callback;
        }

    }

    /**
     * check if the sending message is in the correct format
     *
     * @param isBroadcast
     * @param message
     * @return
     */
    protected abstract boolean checkSendingMessageFormat(boolean isBroadcast, String message);
}
