package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.client;

import rocks.imsofa.physicalsensor.physicalsensorreaderfx.*;
import java.io.File;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.stage.WindowEvent;
import org.apache.commons.io.FileUtils;
import rocks.imsofa.net.simplesocket.SimpleSocket;

/**
 * JavaFX App
 */
public class ClientApp extends Application {

    private static Scene scene;
    private static Application application=null;
    
    public static Application getApplication(){
        return application;
    }

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader newProjectDialogLoader = new FXMLLoader(ClientApp.class.getResource("ClientMain.fxml"));
        Parent newProjectRoot = newProjectDialogLoader.load();
        ClientMainController clientMainController = newProjectDialogLoader.getController();

        Scene scene = new Scene(newProjectRoot, 400, 300);
        Stage dialogStage = stage;
        stage.setTitle("PhysicalSensorReaderFX");
        dialogStage.setScene(scene);
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                //delete temp files
                File homeDirectory = new File(new File(System.getProperty("user.home")), ".physicalsensorreader");
                File tempDirectory = new File(homeDirectory, ".tmp");
                for (File file : tempDirectory.listFiles()) {
                    System.out.println("deleting " + file);
                    try {
                        FileUtils.deleteDirectory(file);
                    } catch (IOException ex) {
                        Logger.getLogger(ClientApp.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                try {
                    clientMainController.shutdown();
                } catch (Exception ex) {
                    Logger.getLogger(ClientApp.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        stage.show();
        
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(ClientApp.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
