/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.handlers;

import java.util.ArrayList;
import java.util.List;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.GlobalContext;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.Message;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.servlets.MessageResponse;

/**
 *
 * @author lendle
 */
public class MessageHandlers {
    private List<MessageHandler> messageHandlers=new ArrayList<>();

    public MessageHandlers(GlobalContext globalContext) {
//        messageHandlers.add(new ClientRegistrationMessageHandler(globalContext.getRemoteSessions()));
//        messageHandlers.add(new SignalDataMessageHandler(globalContext));
    }
    
    public void addMessageHandler(MessageHandler messageHandler){
        messageHandlers.add(messageHandler);
    }
    
    public MessageResponse process(Message message){
        for(MessageHandler messageHandler: messageHandlers){
            if(messageHandler.canHandle(message)){
                return messageHandler.process(message);
            }
        }
        return new MessageResponse(true, -1, "no message handler registered for "+message.getClass().getCanonicalName());
    }
    
}
