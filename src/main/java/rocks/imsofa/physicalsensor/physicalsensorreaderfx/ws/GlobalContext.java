/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws;

import java.io.File;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.handlers.MessageHandlers;

/**
 *
 * @author lendle
 */
public class GlobalContext {
    private RemoteSessions remoteSessions=null;
    private MessageHandlers messageHandlers=null;
    private File rootDirectory=null;
    private IdProvider idProvider=null;

    public IdProvider getIdProvider() {
        return idProvider;
    }

    public void setIdProvider(IdProvider idProvider) {
        this.idProvider = idProvider;
    }
    
    

    public File getRootDirectory() {
        return rootDirectory;
    }

    public void setRootDirectory(File rootDirectory) {
        this.rootDirectory = rootDirectory;
    }
    
    public RemoteSessions getRemoteSessions() {
        return remoteSessions;
    }

    public void setRemoteSessions(RemoteSessions remoteSessions) {
        this.remoteSessions = remoteSessions;
    }

    public MessageHandlers getMessageHandlers() {
        return messageHandlers;
    }

    public void setMessageHandlers(MessageHandlers messageHandlers) {
        this.messageHandlers = messageHandlers;
    }
    
    
    
}
