/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.client;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalData;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalDataListener;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalDataType;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.receiver.SignalDataReceiver;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.MessageServer;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.SignalDataMessage;

/**
 * forward signal data to server
 *
 * @author USER
 */
public class SignalDataForwardListener implements SignalDataListener {

    private MessageServer messageServer = null;
    private SignalDataType signalDataType = null;
    private ClientContext clientContext=null;

    public SignalDataForwardListener(MessageServer messageServer, SignalDataType signalDataType, ClientContext clientContext) {
        this.messageServer = messageServer;
        this.signalDataType = signalDataType;
        this.clientContext=clientContext;
    }

    public void newData(SignalDataReceiver source, SignalData data) {
        try {
            int type = -1;
            switch (signalDataType) {
                case TYPE_FREQUENCY:
                    type = SignalDataMessage.TYPE_FREQUENCY;
                    break;
                case TYPE_GSR:
                    type = SignalDataMessage.TYPE_GSR;
                    break;
                case TYPE_HR:
                    type = SignalDataMessage.TYPE_HR;
                    break;
                case TYPE_VOLUME:
                    type = SignalDataMessage.TYPE_VOLUME;
                    break;
                default:
                    throw new AssertionError();
            }
            messageServer.sendSignalDataMessage(type, data.getTimestamp()+clientContext.getClientServerTimeOffset(), (Double) data.getValue(), null);
        } catch (Exception ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
