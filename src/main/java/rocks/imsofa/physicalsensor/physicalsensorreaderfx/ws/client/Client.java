/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.client;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.Session;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalDataType;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.PropertiesUtil;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.SerializationUtil;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.AsyncTask;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.GlobalContext;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.IdProvider;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.MessageServer;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.Message;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.QueryServerResponseMessage;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.ServerLivenessCheckMessage;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.handlers.CommandMessageHandler;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.handlers.MessageHandler;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.handlers.MessageHandlers;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.servlets.MessageResponse;

/**
 *
 * @author lendle
 */
public class Client extends MessageServer implements IdProvider {

    private ClientContext clientContext = new ClientContext();
    protected Session session = null;
    private File projectDirectory = null;
    private boolean bindingServerAsync = false;
    private Properties props = null;
    private CommandMessageHandler cmh = null;
    private ServerLivenessCheckerDaemon serverLivenessCheckerDaemon = null;

    public Client() throws Exception {
        this(-1);
    }

    public Client(int port) throws Exception {
        this(port, true);
    }

    public Client(int port, boolean autoStart) throws Exception {
        super(SerializationUtil.createTempDirectory(), port, false);
        if (autoStart) {
            initClient();
        }
    }

    protected void initClient() throws Exception {
        projectDirectory = SerializationUtil.createTempDirectory();
        session = new Session();
        session.init(projectDirectory);
        session.addDataListener(SignalDataType.TYPE_HR, new SignalDataForwardListener(this, SignalDataType.TYPE_HR, clientContext));
        session.addDataListener(SignalDataType.TYPE_FREQUENCY, new SignalDataForwardListener(this, SignalDataType.TYPE_FREQUENCY, clientContext));
        session.addDataListener(SignalDataType.TYPE_GSR, new SignalDataForwardListener(this, SignalDataType.TYPE_GSR, clientContext));
        session.addDataListener(SignalDataType.TYPE_VOLUME, new SignalDataForwardListener(this, SignalDataType.TYPE_VOLUME, clientContext));
        globalContext.setIdProvider(this);
    }

    @Override
    protected void initMessageHandlers(GlobalContext globalContext, MessageHandlers messageHandlers) {
        super.initMessageHandlers(globalContext, messageHandlers); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
        cmh = new CommandMessageHandler(globalContext);
        messageHandlers.addMessageHandler(cmh);
        messageHandlers.addMessageHandler(new MessageHandler() {
            @Override
            public boolean canHandle(Message message) {
                return message instanceof QueryServerResponseMessage;
            }

            @Override
            public MessageResponse process(Message message) {
                QueryServerResponseMessage qsrm = (QueryServerResponseMessage) message;
                connectedServerId = qsrm.getSenderId();
                long serverTime = qsrm.getTimestamp();
                long clientTime = System.currentTimeMillis();
//                System.out.println("connectedServerId="+connectedServerId);
                clientContext.setClientServerTimeOffset(serverTime - clientTime);
                return new MessageResponse();
            }
        });
    }

    public void addClientGUICommandEventListener(ClientGUICommandEventListener l) {
        cmh.addClientGUICommandEventListener(l);
    }

    public synchronized void bindServerAsync() {
        bindingServerAsync = true;
        clientContext.setClientStatus(ClientStatus.AWAITING_SERVER);
        new Bind2ServerAsyncTask(this).start(new AsyncTask.Callback() {
            @Override
            public void success() {
//                System.out.println("success");
                clientContext.setClientStatus(ClientStatus.BOUND_SERVER);
                bindingServerAsync = false;
                if (serverLivenessCheckerDaemon == null) {
                    serverLivenessCheckerDaemon = new ServerLivenessCheckerDaemon();
                    serverLivenessCheckerDaemon.start();
                }
            }

            @Override
            public void failed(Throwable error) {
//                System.out.println("failed");
                clientContext.setClientStatus(ClientStatus.STANDALONE);
                bindingServerAsync = false;
                if (serverLivenessCheckerDaemon == null) {
                    serverLivenessCheckerDaemon = new ServerLivenessCheckerDaemon();
                    serverLivenessCheckerDaemon.start();
                }
            }
        });
    }

    @Override
    public void stop() throws Exception {
        super.stop(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
        session.shutdown();
        FileUtils.deleteDirectory(projectDirectory);
    }

    @Override
    public void start() throws Exception {
        super.start(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
        session.start(PropertiesUtil.isTestMode());
        bindServerAsync();
    }

    public Session getSession() {
        return session;
    }

    public ClientContext getClientContext() {
        return clientContext;
    }

    public static void main(String[] args) throws Exception {
        Client client = new Client();
        client.start();
        client.bindServerAsync();
        while (true);
    }

    /**
     * daemon that checks server liveness
     */
    private class ServerLivenessCheckerDaemon extends Thread {

        public ServerLivenessCheckerDaemon() {
            setDaemon(true);
        }

        public void run() {
            while (true) {
                try {
                    if (connectedServerId != null) {
                        sendDirectMessage(new ServerLivenessCheckMessage(), true, new MessageResponseCallback() {
                            @Override
                            public void messageResponseReceived(MessageResponse messageResponse) {
                                if (messageResponse.isError()) {
                                    connectedServerId = null;
                                    clientContext.setClientStatus(ClientStatus.STANDALONE);
                                    bindServerAsync();
                                }
                            }
                        });
                        Thread.sleep(30000);
                        continue;
                    } else if (!bindingServerAsync && clientContext.getClientStatus().equals(ClientStatus.STANDALONE)) {
                        Logger.getLogger(getClass().getName()).info("Restart bindingServerAsync process now...");
                        serverLivenessCheckerDaemon = null;
                        bindServerAsync();
                        break;
                    }
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(MessageServer.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(MessageServer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    @Override
    public String getLocalId() {
        String idPrefix = PropertiesUtil.getIdPrefix();
        if (idPrefix == null) {
            return getLocalIp() + "_" + getLocalPort();
        } else {
            return idPrefix + "_" + super.getLocalPort();
        }
    }

    public void writeIdPrefix(String idPrefix) {
        PropertiesUtil.writeIdPrefix(idPrefix);
    }
}
