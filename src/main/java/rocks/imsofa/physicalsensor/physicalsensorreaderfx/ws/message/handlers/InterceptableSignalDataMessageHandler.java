/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.handlers;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import org.apache.commons.io.FileUtils;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.ThreadUtil;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.GlobalContext;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.Message;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.servlets.MessageResponse;

/**
 *
 * @author lendle
 */
public class InterceptableSignalDataMessageHandler extends SignalDataMessageHandler {

    private List<ScriptEngine> scripts = new ArrayList<>();
    private URLClassLoader uRLClassLoader = null;

    public InterceptableSignalDataMessageHandler(GlobalContext globalContext) {
        super(globalContext);
        File scriptDir = new File(".scripts");
        File libDir = new File(scriptDir, "lib");
        if (scriptDir.exists()) {
            uRLClassLoader = null;
            if (libDir.exists()) {
                File[] libFiles = libDir.listFiles();
                List<URL> urls = new ArrayList<>();
                for (File file : libFiles) {
                    if (file.getName().toLowerCase().endsWith(".jar")) {
                        try {
                            urls.add(file.toURI().toURL());
                        } catch (MalformedURLException ex) {
                            Logger.getLogger(InterceptableSignalDataMessageHandler.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                uRLClassLoader = new URLClassLoader(urls.toArray(new URL[0]));
            }
            ScriptEngineManager factory = (uRLClassLoader != null) ? new ScriptEngineManager(uRLClassLoader) : new ScriptEngineManager();
            ScriptEngineManager sem = new ScriptEngineManager();
            List<ScriptEngineFactory> factories = sem.getEngineFactories();
            for(ScriptEngineFactory scriptEngineFactory : factories){
                System.out.println(scriptEngineFactory.getEngineName());
            }
            for (File scriptFile : scriptDir.listFiles()) {
                try {
                    if (scriptFile.getName().toLowerCase().endsWith(".groovy") == false) {
                        continue;
                    }

                    ScriptEngine engine = (ScriptEngine) factory.getEngineByName("groovy");
                    System.out.println("uRLClassLoader=" + uRLClassLoader + ": engine=" + engine + ", scriptFile=" + scriptFile);
                    engine.eval(FileUtils.readFileToString(scriptFile, "utf-8"));
                    scripts.add(engine);
                } catch (IOException ex) {
                    Logger.getLogger(InterceptableSignalDataMessageHandler.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ScriptException ex) {
                    Logger.getLogger(InterceptableSignalDataMessageHandler.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    @Override
    public synchronized MessageResponse process(Message message) {
        ThreadUtil.submit(() -> {
            for (ScriptEngine engine : scripts) {
                try {
                    Thread.currentThread().setContextClassLoader(uRLClassLoader);
                    engine.put("classLoader", uRLClassLoader);
                    ((Invocable) engine).invokeFunction("process", message);
//                    script.eval();
                } catch (Throwable ex) {
                    Logger.getLogger(InterceptableSignalDataMessageHandler.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });
        return super.process(message); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

}
