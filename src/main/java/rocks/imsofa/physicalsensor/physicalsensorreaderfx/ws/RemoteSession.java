/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws;

import java.io.File;
import rocks.imsofa.pagedrecordlist.PagedRecordCollectionList;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.DataMap;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.PhysicalSensorReaderModel;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.Session;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.SerializationUtil;

/**
 *
 * @author lendle
 */
public class RemoteSession extends Session{
    private String id=null;
    private long lastSeen=System.currentTimeMillis();

    public void init(File projectRootDirectory) throws Exception {
        this.projectRootDirectory = projectRootDirectory;
        this.physicalSensorReaderModel = new WSPhysicalSensorReaderModel(this.projectRootDirectory);
        dataMap.setFrequenciesPageRecordCollectionList(SerializationUtil.newPagedRecordCollectionList(new File(projectRootDirectory, "frequencies_data")));
        dataMap.setVolumesPageRecordCollectionList(SerializationUtil.newPagedRecordCollectionList(new File(projectRootDirectory, "volumes_data")));
        dataMap.setHrPageRecordCollectionList(SerializationUtil.newPagedRecordCollectionList(new File(projectRootDirectory, "hr_data")));
        dataMap.setGSRPageRecordCollectionList(PagedRecordCollectionList.newInstance(new File(projectRootDirectory, "gsr_data")));
    }
    
    public void reset() throws Exception{
//        File newDir=new File(this.projectRootDirectory.getParentFile().getParentFile(), ""+System.currentTimeMillis());
//        newDir=new File(newDir, id);
//        newDir.mkdirs();
//        this.init(newDir);
//        physicalSensorReaderModel.start();
        dataMap.setFrequenciesPageRecordCollectionList(SerializationUtil.newPagedRecordCollectionList(new File(projectRootDirectory, "frequencies_data"+System.currentTimeMillis())));
        dataMap.setVolumesPageRecordCollectionList(SerializationUtil.newPagedRecordCollectionList(new File(projectRootDirectory, "volumes_data"+System.currentTimeMillis())));
        dataMap.setHrPageRecordCollectionList(SerializationUtil.newPagedRecordCollectionList(new File(projectRootDirectory, "hr_data"+System.currentTimeMillis())));
        dataMap.setGSRPageRecordCollectionList(PagedRecordCollectionList.newInstance(new File(projectRootDirectory, "gsr_data"+System.currentTimeMillis())));
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(long lastSeen) {
        this.lastSeen = lastSeen;
    } 
}
