/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.client;

import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.Message;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.servlets.MessageResponse;

/**
 *
 * @author lendle
 */
public interface ClientMessageCallback {
    public void callback(Message message, MessageResponse messageResponse);
}
