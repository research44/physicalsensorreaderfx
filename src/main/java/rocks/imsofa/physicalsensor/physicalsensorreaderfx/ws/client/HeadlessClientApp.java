/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.client;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalData;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalDataListener;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalDataType;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.receiver.SignalDataReceiver;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.IPUtil;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.ThreadUtil;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.CommandMessage;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.messagebus.AbstractJettyMessageBus;

/**
 *
 * @author lendle
 */
public class HeadlessClientApp extends Client implements ClientGUICommandEventListener {

    private double gsr = -1, hr = -1;

    public HeadlessClientApp(int port) throws Exception {
        super(port, false);
        ServletContextHandler handler = ((ServletContextHandler) (((AbstractJettyMessageBus) messageBus).getServer()).getHandler());
        handler.addServlet(new ServletHolder(new ClientUIServlet()), "/ui");
        handler.addServlet(new ServletHolder(new ClientDataServlet()), "/data");
        handler.addServlet(new ServletHolder(new ClientCommandServlet()), "/command");

        ThreadUtil.submit(() -> {
            while (true) {
                //wait until network is ready
                String ip = IPUtil.getIntranetIP();
                if (ip != null) {
                    break;
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(HeadlessClientApp.class.getName()).log(Level.SEVERE, null, ex);
                }
                Logger.getLogger(getClass().getName()).info("network is not ready yet, waiting ...");
            }
            try {
                Logger.getLogger(getClass().getName()).info("network is ready, starting ...");
                HeadlessClientApp.this.initClient();
                session.addDataListener(SignalDataType.TYPE_HR, new SignalDataListener() {
                    @Override
                    public void newData(SignalDataReceiver source, SignalData data) {
                        hr = (Double) data.getValue();
                    }
                });
                session.addDataListener(SignalDataType.TYPE_GSR, new SignalDataListener() {
                    @Override
                    public void newData(SignalDataReceiver source, SignalData data) {
                        gsr = (Double) data.getValue();
                    }
                });
                
                HeadlessClientApp.this.start();
            } catch (Exception ex) {
                Logger.getLogger(HeadlessClientApp.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        int port = 8888;
        if (args.length == 1) {
            port = Integer.valueOf(args[0]);
        }
        Thread.sleep(10000);
        new HeadlessClientApp(port);
    }

    @Override
    public void commandReceived(ClientGUICommand command) {
        if (command instanceof BeepCommand) {
            //do nothing
            return;
        } else if (command instanceof RenameCommand) {
            RenameCommand renameCommand = (RenameCommand) command;
            this.writeIdPrefix(renameCommand.getNewName());
        }
    }

    class ClientUIServlet extends HttpServlet {

        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            resp.setContentType("text/html;charset=utf-8");
            resp.addHeader("Access-Control-Allow-Origin", "*");
            resp.getWriter().println("hr: <span id='hr'></span>" + "<br/>gsr: <span id='gsr'></span><br/><a href='command?name=reboot'>Reboot</a>&nbsp;&nbsp;<a href='command?name=shutdown'>Shutdown</a>&nbsp;&nbsp;<a href='command?name=kill'>Kill</a>");
            resp.getWriter().println("<script>");
            resp.getWriter().println("");
            //resp.getWriter().println("let o=await fetch('data', {method: 'get', headers: {'Content-Type':'application/json', 'Accept':'application/json'}}).then((response)=>{  let o=response.text();return o;  });");
            resp.getWriter().println("async function getValue(){let p=await fetch('data', {method: 'get', headers: {'Content-Type':'application/json', 'Accept':'application/json'}}); let json=await p.json(); document.getElementById('hr').textContent=json.hr;document.getElementById('gsr').textContent=json.gsr;}");
            resp.getWriter().println("setInterval(getValue, 1000);");
            resp.getWriter().println("</script>");
            resp.getWriter().flush();
        }
    }

    class ClientDataServlet extends HttpServlet {

        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            resp.addHeader("Access-Control-Allow-Origin", "*");
            resp.setContentType("application/json;charset=utf-8");
            resp.getWriter().print("{\"hr\":" + hr + ", \"gsr\":" + gsr + "}");
            resp.getWriter().flush();
        }
    }

    class ClientCommandServlet extends HttpServlet {

        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            resp.addHeader("Access-Control-Allow-Origin", "*");
            resp.setContentType("application/json;charset=utf-8");
            String command = req.getParameter("name");

            if ("reboot".equals(command)) {
                CommandMessage cm = new CommandMessage();
                cm.setCommand("reboot");
                messageHandlers.process(cm);
            } else if ("shutdown".equals(command)) {
                CommandMessage cm = new CommandMessage();
                cm.setCommand("shutdown");
                messageHandlers.process(cm);
            } else if ("kill".equals(command)) {
                CommandMessage cm = new CommandMessage();
                cm.setCommand("kill");
                try {
                    HeadlessClientApp.this.stop();
                    ThreadUtil.shutdownNow();
                } catch (Exception ex) {
                    Logger.getLogger(HeadlessClientApp.class.getName()).log(Level.SEVERE, null, ex);
                }
                messageHandlers.process(cm);
            }
            resp.getWriter().flush();
        }
    }
}
