/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws;

/**
 *
 * @author lendle
 */
public class Constants {
    public static final String MULTICAST_GROUP="224.0.0.100";
    public static final int MULTICAST_PORT=5000;
}
