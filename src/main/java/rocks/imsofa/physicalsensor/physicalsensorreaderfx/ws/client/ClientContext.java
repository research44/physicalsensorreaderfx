/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.client;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lendle
 */
public class ClientContext {
    private String serverIp=null;
    private long clientServerTimeOffset=0;//client time + offset = server time
    private int serverPort=-1;
    private String clientId=null;
    private ClientStatus clientStatus=ClientStatus.STANDALONE;
    private List<ClientStatusChangeListener> clientStatusChangeListeners=new ArrayList<>();
    
    public void addClientStatusChangeListener(ClientStatusChangeListener l){
        this.clientStatusChangeListeners.add(l);
    }

    public synchronized String getServerIp() {
        return serverIp;
    }

    public synchronized void setServerIp(String serverIp) {
        this.serverIp = serverIp;
    }

    public int getServerPort() {
        return serverPort;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    public synchronized String getClientId() {
        return clientId;
    }

    public synchronized void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public synchronized ClientStatus getClientStatus() {
        return clientStatus;
    }

    public synchronized void setClientStatus(ClientStatus clientStatus) {
        ClientStatus oldStatus=this.clientStatus;
        this.clientStatus = clientStatus;
        for(ClientStatusChangeListener l: clientStatusChangeListeners){
            l.onStateChanged(oldStatus, this.clientStatus);
        }
    }

    public long getClientServerTimeOffset() {
        return clientServerTimeOffset;
    }

    public void setClientServerTimeOffset(long clientServerTimeOffset) {
        this.clientServerTimeOffset = clientServerTimeOffset;
    }
    
    
}
