/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.messagebus;

import java.util.List;

/**
 *
 * @author lendle
 */
public interface MessageBus {

    public String getLocalId();

    public void sendBroadcastMessage(String message) throws Exception;

    /**
     *
     * @param remoteId
     * @param allowColoading if true, the message can be co-loaded with other
     * messages to the same remote id
     * @param message
     * @param messageCallback the messageCallback to receive response
     * @throws Exception
     */
    public void sendDirectMessage(String remoteId, boolean allowColoading, String message, MessageCallback messageCallback) throws Exception;

    public void start() throws Exception;

    public void stop() throws Exception;

    /**
     * a broadcast message was received
     *
     * @param message
     */
    public void broadcastMessageReceived(String message);

    /**
     * a direct message (to this id) was received
     *
     * @param remoteId
     * @param message
     * @return
     */
    public String singleDirectMessageReceived(String remoteId, String message);
    /**
     * multiple direct messages (to this id) were received
     *
     * @param remoteId
     * @param message
     * @return
     */
    public List<String> coLoadedDirectMessagesReceived(String remoteId, List<String> message);

    public static interface MessageCallback {

        public void responseReceived(String remoteId, boolean coloaded, String response);
        public void failed(String remoteId, boolean coloaded, Throwable e);
    }
}
