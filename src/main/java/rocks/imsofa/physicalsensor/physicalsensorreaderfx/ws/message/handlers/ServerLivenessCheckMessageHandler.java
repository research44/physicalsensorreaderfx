/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.handlers;

import java.util.logging.Logger;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalData;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.RemoteSession;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.GlobalContext;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.Message;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.ServerLivenessCheckMessage;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.SignalDataMessage;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.servlets.MessageResponse;

/**
 * received a message from external data sources
 * and store in local data sink
 * @author lendle
 */
public class ServerLivenessCheckMessageHandler implements MessageHandler {
    private GlobalContext globalContext=null;

    public ServerLivenessCheckMessageHandler(GlobalContext globalContext) {
        this.globalContext=globalContext;
    }
    
    
    @Override
    public boolean canHandle(Message message) {
        return message instanceof ServerLivenessCheckMessage;
    }

    @Override
    public MessageResponse process(Message message) {
        //TODO: put signal into session DataMap
        ServerLivenessCheckMessage serverLivenessCheckMessage=(ServerLivenessCheckMessage) message;
        return new MessageResponse();
    }
    
}
