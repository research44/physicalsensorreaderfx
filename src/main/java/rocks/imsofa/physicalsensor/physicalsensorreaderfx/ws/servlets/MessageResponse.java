/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.servlets;

import java.util.Map;

/**
 *
 * @author lendle
 */
public class MessageResponse {
    private boolean error=false;
    private int code=-1;
    private String message=null;
    private Map<String, String> data=null;
    private long timestamp=System.currentTimeMillis();

    public MessageResponse() {
    }
    
    public MessageResponse(boolean error) {
        this.error=error;
    }
    
    public MessageResponse(boolean error, int code, String message) {
        this.error=error;
        this.message=message;
        this.code=code;
    }
    
    public MessageResponse(boolean error, int code, String message, Map<String, String> data) {
        this.error=error;
        this.message=message;
        this.data=data;
        this.code=code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    
    
    public Map<String, String> getData() {
        return data;
    }

    public void setData(Map<String, String> data) {
        this.data = data;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
    
    
    
    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
}
