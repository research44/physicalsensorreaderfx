/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws;

import com.google.gson.Gson;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.IPUtil;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.AbstractMessage;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.ClientRegistrationMessage;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.CommandMessage;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.Message;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.QueryServerRequestMessage;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.QueryServerResponseMessage;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.SerializedMessage;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.SignalDataMessage;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.handlers.MessageHandler;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.handlers.MessageHandlers;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.servlets.MessageResponse;

/**
 * a server for receiving http messages customize messageHandlers to configure
 * installed message handlers
 *
 * @author USER
 */
public class MessageServer {

    protected RemoteSessions remoteSessions = null;
    protected MessageBus messageBus = null;
    protected String connectedServerId = null;
    private Gson gson = new Gson();
    protected GlobalContext globalContext = new GlobalContext();
    protected MessageHandlers messageHandlers = null;
    private boolean messageHandlersInitialized = false;

    public MessageServer(File rootDirectory, int port, boolean server) throws Exception {
        remoteSessions = new RemoteSessions(rootDirectory);
        globalContext.setRootDirectory(rootDirectory);
        globalContext.setRemoteSessions(remoteSessions);
        messageBus = new MessageBus(globalContext, server, port);
        messageHandlers = new MessageHandlers(globalContext);
        globalContext.setMessageHandlers(messageHandlers);
    }

    public String getConnectedServerId() {
        return connectedServerId;
    }

    protected synchronized void initMessageHandlers(GlobalContext globalContext, MessageHandlers messageHandlers) {
        messageHandlersInitialized=true;
    }

    public synchronized void start() throws Exception {
        if (!messageHandlersInitialized) {
            initMessageHandlers(globalContext, messageHandlers);
            messageHandlersInitialized=true;
        }
        
        messageBus.start();
    }

    public void stop() throws Exception {
        messageBus.stop();
    }

    public RemoteSessions getRemoteSessions() {
        return remoteSessions;
    }

    public void sendDirectMessage(AbstractMessage message, String remoteId, boolean coloading, MessageResponseCallback callback) throws Exception {
        SerializedMessage serializedMessage = new SerializedMessage();
        serializedMessage.setJson(gson.toJson(message));
        serializedMessage.setType(message.getClass().getCanonicalName());
        messageBus.sendDirectMessage(remoteId, coloading, gson.toJson(serializedMessage), new rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.messagebus.MessageBus.MessageCallback() {
            @Override
            public void responseReceived(String remoteId, boolean coloaded, String response) {
                if (callback != null) {
                    callback.messageResponseReceived(gson.fromJson(response, MessageResponse.class));
                }
            }

            @Override
            public void failed(String remoteId, boolean coloaded, Throwable e) {
                if (callback != null) {
                    callback.messageResponseReceived(new MessageResponse(true, -1, "" + e));
                }
            }
        });
    }

    public void sendDirectMessage(AbstractMessage message, boolean coloading, MessageResponseCallback callback) throws Exception {
        this.sendDirectMessage(message, connectedServerId, coloading, callback);
    }

    public void sendSignalDataMessage(SignalDataMessage signalDataMessage, MessageResponseCallback messageCallback) throws Exception {
        sendDirectMessage(signalDataMessage, true, messageCallback);
    }

    public void sendSignalDataMessage(int signalType, long timestamp, double value, MessageResponseCallback messageCallback) throws Exception {
        SignalDataMessage message = new SignalDataMessage();
        message.setSenderId(messageBus.getLocalId());
        message.setMessageId("" + UUID.randomUUID().toString());
        message.setTimestamp(timestamp);
        message.setSignalType(signalType);
        message.setValue(value);
        message.setSenderIp(IPUtil.getIntranetIP());
        message.setSenderPort(this.messageBus.getLocalPort());
        sendSignalDataMessage(message, messageCallback);
    }

    public void sendShutdownMessage(String remoteId) throws Exception {
        CommandMessage commandMessage = new CommandMessage();
        commandMessage.setCommand("shutdown");
        commandMessage.setSenderId(getLocalId());
        commandMessage.setMessageId("" + UUID.randomUUID().toString());
        commandMessage.setSenderIp(getLocalIp());
        commandMessage.setSenderPort(getLocalPort());
        commandMessage.setTimestamp(System.currentTimeMillis());
        this.sendDirectMessage(commandMessage, remoteId, false, null);
    }

    public void sendBeepMessage(String remoteId) throws Exception {
        CommandMessage commandMessage = new CommandMessage();
        commandMessage.setCommand("beep");
        commandMessage.setSenderId(getLocalId());
        commandMessage.setMessageId("" + UUID.randomUUID().toString());
        commandMessage.setSenderIp(getLocalIp());
        commandMessage.setSenderPort(getLocalPort());
        commandMessage.setTimestamp(System.currentTimeMillis());
        this.sendDirectMessage(commandMessage, remoteId, false, null);
    }
    
    public void sendRenameMessage(String remoteId, String newName) throws Exception {
        CommandMessage commandMessage = new CommandMessage();
        commandMessage.setCommand("rename");
        commandMessage.setSenderId(getLocalId());
        commandMessage.setMessageId("" + UUID.randomUUID().toString());
        commandMessage.setSenderIp(getLocalIp());
        commandMessage.setSenderPort(getLocalPort());
        commandMessage.setTimestamp(System.currentTimeMillis());
        commandMessage.getArgs().add(newName);
        this.sendDirectMessage(commandMessage, remoteId, false, null);
    }

    public void sendClientRegistrationMessage(ClientRegistrationMessage clientRegistrationMessage, MessageResponseCallback messageCallback) throws Exception {
        sendDirectMessage(clientRegistrationMessage, false, messageCallback);
    }

    public void sendClientRegistrationMessage(MessageResponseCallback messageCallback) throws Exception {
        ClientRegistrationMessage message = new ClientRegistrationMessage();
        message.setSenderId(messageBus.getLocalId());
        message.setMessageId("" + UUID.randomUUID().toString());
        message.setTimestamp(System.currentTimeMillis());
        message.setSenderIp(IPUtil.getIntranetIP());
        message.setSenderPort(this.messageBus.getLocalPort());
        sendClientRegistrationMessage(message, messageCallback);
    }

    public void sendQueryServerRequestMessage(QueryServerRequestMessage queryServerRequestMessage) throws Exception {
        SerializedMessage serializedMessage = new SerializedMessage();
        serializedMessage.setJson(gson.toJson(queryServerRequestMessage));
        serializedMessage.setType(QueryServerRequestMessage.class.getCanonicalName());
        messageBus.sendBroadcastMessage(gson.toJson(serializedMessage));
    }

    public void sendQueryServerRequestMessage() throws Exception {
        QueryServerRequestMessage qsrm = new QueryServerRequestMessage();
        qsrm.setSenderId(getLocalId());
        qsrm.setMessageId("" + UUID.randomUUID().toString());
        qsrm.setSenderIp(getLocalIp());
        qsrm.setSenderPort(getLocalPort());
        this.sendQueryServerRequestMessage(qsrm);
    }

    public int getLocalPort() {
        return messageBus.getLocalPort();
    }

    public String getLocalId() {
        return messageBus.getLocalId();
    }

    public String getLocalIp() {
        return messageBus.getLocalIp();
    }

    public static interface MessageResponseCallback {

        public void messageResponseReceived(MessageResponse messageResponse);
    }
}
