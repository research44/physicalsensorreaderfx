/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.jetty.server.ServerConnector;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.IPUtil;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.AbstractMessage;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.Message;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.MessageDeserializer;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.QueryServerRequestMessage;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.QueryServerResponseMessage;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.SerializedMessage;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.messagebus.AbstractJettyMessageBus;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.servlets.MessageResponse;

/**
 * direct message must be in SerializedMessage format
 *
 * @author USER
 */
public class MessageBus extends AbstractJettyMessageBus {

    private GlobalContext globalContext = null;
    private boolean isServer = false;
    private Map<String, EndpointInfo> endPoints = new HashMap<>();
    /**
     * 
     * @param globalContext
     * @param isServer
     * @param port -1 for random port
     */
    public MessageBus(GlobalContext globalContext, boolean isServer, int port) {
        super(port);
        this.globalContext = globalContext;
        this.isServer = isServer;
    }

    @Override
    protected String getEndpoint(String remoteId, boolean coloadingMessages) {
        EndpointInfo endpointInfo = endPoints.get(remoteId);
        if (endpointInfo != null) {
            if (coloadingMessages) {
                return "http://" + endpointInfo.getIp() + ":" + endpointInfo.getPort() + "/multimessage";
            } else {
                return "http://" + endpointInfo.getIp() + ":" + endpointInfo.getPort() + "/message";
            }
        } else {
            return null;
        }
    }

    @Override
    public void broadcastMessageReceived(String message) {
        try {
            Logger.getLogger(MessageBus.class.getName()).info("broadcastMessageReceived: " + message);
            Gson gson = new Gson();
            SerializedMessage serializedMessage = gson.fromJson(message, SerializedMessage.class);
            //this is the only possible type of message
            QueryServerRequestMessage request = (QueryServerRequestMessage) MessageDeserializer.deserialize(serializedMessage);

            EndpointInfo endpointInfo = new EndpointInfo();
            endpointInfo.setId(request.getSenderId());
            endpointInfo.setIp(request.getSenderIp());
            endpointInfo.setPort(request.getSenderPort());
            endPoints.put(request.getSenderId(), endpointInfo);
            if (this.isServer && request.getSenderId().equals(this.getLocalId())==false) {
                //then reply
                //prepare reply
                QueryServerResponseMessage response = new QueryServerResponseMessage();
                response.setMessageId(""+UUID.randomUUID().toString());
                response.setSenderId(this.getLocalId());
                response.setSenderIp(IPUtil.getIntranetIP());
                response.setSenderPort(((ServerConnector) (this.server.getConnectors()[0])).getLocalPort());
                SerializedMessage m = new SerializedMessage();
                m.setJson(gson.toJson(response));
                m.setType(QueryServerResponseMessage.class.getCanonicalName());
                this.sendDirectMessage(request.getSenderId(), false, gson.toJson(m), new MessageCallback() {
                    @Override
                    public void responseReceived(String remoteId, boolean coloaded, String response) {
                        Logger.getLogger(MessageBus.class.getName()).info(response);
                    }

                    @Override
                    public void failed(String remoteId, boolean coloaded, Throwable e) {
                        Logger.getLogger(MessageBus.class.getName()).log(Level.SEVERE, null, e);
                    }
                });
//                Logger.getLogger(MessageBus.class.getName()).info("server response sent");
            }
        } catch (Exception ex) {
            Logger.getLogger(MessageBus.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String singleDirectMessageReceived(String remoteId, String message) {
        try {
            Logger.getLogger(MessageBus.class.getName()).info("singleDirectMessageReceived: " + message);
            return processMessage(message);
        } catch (Exception ex) {
            Logger.getLogger(MessageBus.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    private String processMessage(String message) throws Exception, JsonSyntaxException {
        Gson gson = new Gson();
        SerializedMessage serializedMessage = gson.fromJson(message, SerializedMessage.class);
        Message deserializedMessage = MessageDeserializer.deserialize(serializedMessage);
        if(deserializedMessage instanceof AbstractMessage){
            AbstractMessage abstractMessage=(AbstractMessage) deserializedMessage;
            EndpointInfo endpointInfo = new EndpointInfo();
            endpointInfo.setId(abstractMessage.getSenderId());
            endpointInfo.setIp(abstractMessage.getSenderIp());
            endpointInfo.setPort(abstractMessage.getSenderPort());
            endPoints.put(abstractMessage.getSenderId(), endpointInfo);
        }
        
        MessageResponse response = globalContext.getMessageHandlers().process(deserializedMessage);
        return gson.toJson(response);
    }

    @Override
    public List<String> coLoadedDirectMessagesReceived(String remoteId, List<String> message) {
        Logger.getLogger(MessageBus.class.getName()).info("coLoadedDirectMessagesReceived: " + message);
        List<String> ret = new ArrayList<>();
        for (String m : message) {
            try {
                ret.add(processMessage(m));
            } catch (Exception ex) {
                Logger.getLogger(MessageBus.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }
        return ret;
    }

    @Override
    protected boolean checkSendingMessageFormat(boolean isBroadcast, String message) {
        Gson gson = new Gson();
        try {
            gson.fromJson(message, SerializedMessage.class);
        } catch (Throwable e) {
            return false;
        }
        return true;
    }

    @Override
    public String getLocalId() {
        if(globalContext.getIdProvider()!=null){
            return globalContext.getIdProvider().getLocalId();
        }else{
            return super.getLocalId();
        }
    }

    
}
