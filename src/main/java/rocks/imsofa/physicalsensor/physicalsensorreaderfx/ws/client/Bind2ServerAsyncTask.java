/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.client;

import java.util.logging.Level;
import java.util.logging.Logger;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.AsyncTask;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.MessageServer;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.servlets.MessageResponse;

/**
 *
 * @author USER
 */
public class Bind2ServerAsyncTask extends AsyncTask {

    private MessageServer messageServer = null;
    private static final long MAX_WAIT_SERVERIP = 10000;

    public Bind2ServerAsyncTask(MessageServer messageServer) {
        this.messageServer = messageServer;
    }

    @Override
    protected void run(Callback callback) {
        try {
            Logger.getLogger(this.getClass().getName()).info("starting Bind2ServerAsyncTask...");
            long startTime = System.currentTimeMillis();
//            System.out.println("startTime="+startTime);
            boolean broadcastSent = false;
            while (true) {
//                System.out.println("localport="+messageServer.getLocalPort());
//                System.out.println("time="+System.currentTimeMillis());
//                System.out.println("messageServer.getConnectedServerId()="+messageServer.getConnectedServerId());
                if (messageServer.getConnectedServerId() != null) {
                    Logger.getLogger(this.getClass().getName()).info("messageServer connected to "+messageServer.getConnectedServerId());
                    break;
                } else {
                    Logger.getLogger(this.getClass().getName()).info("messageServer not connected");
                    if (!broadcastSent) {
                        messageServer.sendQueryServerRequestMessage();
                        broadcastSent = true;
                    }
                    long time = System.currentTimeMillis();
                    if ((time - startTime) > MAX_WAIT_SERVERIP) {
                        callback.failed(new Exception("time out"));
                        return;
                    }
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Bind2ServerAsyncTask.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            this.messageServer.sendClientRegistrationMessage(new MessageServer.MessageResponseCallback() {
                @Override
                public void messageResponseReceived(MessageResponse messageResponse) {
                    if (messageResponse.isError()) {
                        callback.failed(new Exception(messageResponse.getMessage()));
                    } else {
                        callback.success();
                    }
                }
            });
        } catch (Exception ex) {
            Logger.getLogger(Bind2ServerAsyncTask.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
