/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalData;

/**
 *
 * @author lendle
 */
public class TestServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out=resp.getWriter();
        ServletContext context=this.getServletContext();
        Map<String, RemoteSession> remoteSessions=(Map<String, RemoteSession>) context.getAttribute("remoteSessions");
        RemoteSession clientSession=remoteSessions.get("test");
        clientSession.getModel().getFrequencySignalDataReceiver().putSignalData(new SignalData(System.currentTimeMillis(), Math.random()));
    }
    
}
