/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.handlers;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalData;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalDataListener;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalDataType;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.receiver.SignalDataReceiver;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.SerializationUtil;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.RemoteSession;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.RemoteSessions;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.ClientRegistrationMessage;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.Message;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.servlets.MessageResponse;

/**
 *
 * @author lendle
 */
public class ClientRegistrationMessageHandler implements MessageHandler {

    private RemoteSessions remoteSessions = null;

    public ClientRegistrationMessageHandler(RemoteSessions remoteSessions) {
        this.remoteSessions = remoteSessions;
    }

    @Override
    public boolean canHandle(Message message) {
        return message instanceof ClientRegistrationMessage;
    }

    @Override
    public MessageResponse process(Message message) {
        try {
            File tempDirectory = new File(remoteSessions.getRootDirectory(), message.getSenderId());
            RemoteSession remoteSession = new RemoteSession();
            remoteSession.init(tempDirectory);
            remoteSession.setId(message.getSenderId());
            remoteSessions.register(message.getSenderId(), remoteSession);
//            remoteSession.start();
            Logger.getLogger(ClientRegistrationMessageHandler.class.getName()).log(Level.INFO, "client " + message.getSenderId() + " registered");
            return new MessageResponse(false);
        } catch (Exception ex) {
            Logger.getLogger(ClientRegistrationMessageHandler.class.getName()).log(Level.SEVERE, null, ex);
            return new MessageResponse(true, -1, ex.getMessage());
        }
    }

}
