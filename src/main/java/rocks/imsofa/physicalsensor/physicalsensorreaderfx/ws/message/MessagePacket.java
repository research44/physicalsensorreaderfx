/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message;

import java.util.List;

/**
 * message between ws client and server
 * @author lendle
 * @deprecated 
 */
public class MessagePacket {
    private String clientId=null;
    private long timestamp=-1;
    private List<String> jsons=null;
    private List<String> types=null;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public List<String> getJsons() {
        return jsons;
    }

    public void setJsons(List<String> jsons) {
        this.jsons = jsons;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    @Override
    public String toString() {
        return "MessagePacket{" + "clientId=" + clientId + ", timestamp=" + timestamp + ", jsons=" + jsons + ", types=" + types + '}';
    }

 
    
}
