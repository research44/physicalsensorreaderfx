/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws;

import java.io.File;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.handlers.ClientRegistrationMessageHandler;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.handlers.InterceptableSignalDataMessageHandler;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.handlers.MessageHandlers;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.handlers.ServerLivenessCheckMessageHandler;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.multicast.MulticastReceiver;

/**
 *
 * @author lendle
 */
public class PhysicalSensorReaderServer extends MessageServer {

    private MulticastReceiver multicastReceiver = null;

    public PhysicalSensorReaderServer(File rootDirectory, int port) throws Exception {
        super(rootDirectory, port, true);
    }

    @Override
    protected void initMessageHandlers(GlobalContext globalContext, MessageHandlers messageHandlers) {
        messageHandlers.addMessageHandler(new ClientRegistrationMessageHandler(globalContext.getRemoteSessions()));
        messageHandlers.addMessageHandler(new InterceptableSignalDataMessageHandler(globalContext));
        messageHandlers.addMessageHandler(new ServerLivenessCheckMessageHandler(globalContext));
    }

}
