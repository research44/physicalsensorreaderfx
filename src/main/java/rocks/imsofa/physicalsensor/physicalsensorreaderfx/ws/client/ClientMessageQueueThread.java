/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.client;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.Message;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.MessagePacket;

/**
 *
 * @author lendle
 */
public class ClientMessageQueueThread extends Thread {

    private ClientMessageQueue clientMessageQueue = null;
    private String serverIp = null;
    private String clientId=null;
    private ClientContext clientContext=null;

    public ClientMessageQueueThread(ClientContext clientContext, ClientMessageQueue clientMessageQueue) {
        this.clientContext=clientContext;
        this.clientMessageQueue = clientMessageQueue;
        this.setDaemon(true);
    }

    public void run() {
        while (true) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                Logger.getLogger(ClientMessageQueueThread.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (clientContext.getServerIp() != null && clientContext.getClientId()!=null && !clientMessageQueue.isEmpty()) {
                Gson gson = new Gson();
                MessagePacket messagePacket = new MessagePacket();
                messagePacket.setClientId(clientId);
                messagePacket.setTimestamp(System.currentTimeMillis());
                List<Message> messages = clientMessageQueue.fetchAll();
                List<String> types = new ArrayList<>();
                List<String> jsons = new ArrayList<>();
                for (Message message : messages) {
                    types.add(message.getClass().getCanonicalName());
                    jsons.add(gson.toJson(message));
                }
                messagePacket.setTypes(types);
                messagePacket.setJsons(jsons);
                String ret = HttpRequest.post("http://"+clientContext.getServerIp()+":"+clientContext.getServerPort()+"/message").contentType("application/json", "utf-8").send(gson.toJson(messagePacket)).body();
                System.out.println(ret);
            }
        }
    }
}
