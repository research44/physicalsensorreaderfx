/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.client;

/**
 *
 * @author lendle
 */
public class ClientGUICommand {
    protected String name=null;

    public ClientGUICommand(String name) {
        this.name=name;
    }
    
    public String getName() {
        return name;
    }
}
