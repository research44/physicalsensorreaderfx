/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.client;

import com.google.gson.Gson;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.IPUtil;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.multicast.MulticastReceiver;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.multicast.MulticastUtil;

/**
 *
 * @author lendle
 */
public class ClientOld {
    private ClientContext clientContext = new ClientContext();
    private ClientMessageSender clientMessageSender = null;

    public ClientOld() {
        String id = IPUtil.getIntranetIP();
        clientContext.setClientId(id);
        clientContext.setServerIp("localhost");
    }
    
    public void queryServerIpAsync(){
        Thread t=new Thread(){
            public void run(){
                MulticastReceiver receiver=new MulticastReceiver(new MulticastReceiver.Callback(){
                    @Override
                    public void received(String message) {
                        Gson gson=new Gson();
                        Map<String, String> ret=gson.fromJson(message, Map.class);
                        if(ret.get("type").equals("queryServerReply")){
                            String serverIp=ret.get("ip");
                            int port=Integer.valueOf(ret.get("port"));
                            clientContext.setClientStatus(ClientStatus.BOUND_SERVER);
                            clientContext.setServerIp(serverIp);
                            clientContext.setServerPort(port);
                            System.out.println("serverIp="+serverIp);
                        }
                    }
                });
                receiver.start();
                while(clientContext.getClientStatus().equals(ClientStatus.AWAITING_SERVER)){
                    MulticastUtil.multicast("{'type':'queryServer'}");
                    System.out.println("send out query at "+new Date());
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(ClientOld.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        };
        t.setDaemon(true);
        t.start();
    }
    
    public static void main(String [] args) throws Exception{
        ClientOld client=new ClientOld();
        client.clientContext.setClientStatus(ClientStatus.AWAITING_SERVER);
        client.queryServerIpAsync();
        while(1==1);
    }
}
