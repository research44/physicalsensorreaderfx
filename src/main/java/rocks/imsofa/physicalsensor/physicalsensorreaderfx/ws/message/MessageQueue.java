/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lendle
 */
public class MessageQueue {
    private List<Message> messages=new ArrayList<>();
    
    public synchronized void addMessage(Message message){
        this.messages.add(message);
        this.notifyAll();
    }
    
    public synchronized Message getMessage(){
        while(this.messages.isEmpty()){
            try {
                wait();
            } catch (InterruptedException ex) {
                Logger.getLogger(MessageQueue.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return this.messages.remove(0);
    }
}
