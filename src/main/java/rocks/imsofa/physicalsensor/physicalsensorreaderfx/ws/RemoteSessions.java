/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author lendle
 */
public class RemoteSessions {
    private Map<String, RemoteSession> map=new HashMap<>();
    private List<RemoteSessionListener> listeners=new ArrayList<>();
    private File rootDirectory=null;

    public RemoteSessions(File rootDirectory) {
        this.rootDirectory=rootDirectory;
    }

    public File getRootDirectory() {
        return rootDirectory;
    }
    
    public void register(String id, RemoteSession clientSession){
        this.map.put(id, clientSession);
        for(RemoteSessionListener l : listeners){
            l.clientRegistered(clientSession);
        }
    }
    
    public RemoteSession getRemoteSession(String id){
        return map.get(id);
    }
    
    public void addRemoteSessionListener(RemoteSessionListener l){
        this.listeners.add(l);
    }
    
    public static interface RemoteSessionListener{
        public void clientRegistered(RemoteSession remoteSession);
    }
    
    public List<RemoteSession> getSessions(){
        return new ArrayList<>(map.values());
    }
}
