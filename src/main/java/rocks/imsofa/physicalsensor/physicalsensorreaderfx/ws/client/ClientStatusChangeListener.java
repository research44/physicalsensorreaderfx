/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.client;

/**
 *
 * @author lendle
 */
public interface ClientStatusChangeListener {
    public void onStateChanged(ClientStatus oldState, ClientStatus newState);
}
