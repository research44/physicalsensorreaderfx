/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.client;

import java.util.HashMap;
import java.util.Map;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.Message;

/**
 * send client message to server
 *
 * @author lendle
 */
public class ClientMessageSender {

    private ClientMessageQueue clientMessageQueue = new ClientMessageQueue();
    private ClientMessageQueueThread clientMessageQueueThread = null;
    private Map<String, ClientMessageCallback> messageId2CallbackMap = new HashMap<>();
    private ClientContext clientContext = null;

    public ClientMessageSender(ClientContext clientContext) {
        this.clientContext = clientContext;
        clientMessageQueueThread = new ClientMessageQueueThread(clientContext, this.clientMessageQueue);
        clientMessageQueueThread.start();
    }

    public void sendMessage(Message message, ClientMessageCallback callback) {
        if (clientContext.getClientId() == null || clientContext.getServerIp() == null) {
            return;//simply drop the message when no client id or server ip present
        }
        messageId2CallbackMap.put(message.getSenderId(), callback);
        clientMessageQueue.addMessage(message);
    }
}
