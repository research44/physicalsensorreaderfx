/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws;

import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.SerializationUtil;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.QueryServerRequestMessage;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.SignalDataMessage;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.servlets.MessageResponse;

/**
 *
 * @author USER
 */
public class TestClient extends MessageServer{
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        new TestClient().startClient();
    }

    public TestClient() throws Exception {
        super(SerializationUtil.createTempDirectory(), -1, false);
    }
    
    protected void startClient() throws Exception{
        ExecutorService es = Executors.newFixedThreadPool(3);
        es.submit(()->{
            while(true){
                Thread.sleep(3000);
                
                if(connectedServerId!=null){
                    this.sendSignalDataMessage(SignalDataMessage.TYPE_FREQUENCY, System.currentTimeMillis(), Math.random(), null);
                }
            }
        });
        es.submit(() -> {
            try {
                Thread.sleep(3000);
                QueryServerRequestMessage qsrm = new QueryServerRequestMessage();
                qsrm.setSenderId(getLocalId());
                qsrm.setMessageId(""+UUID.randomUUID().toString());
                qsrm.setSenderIp("localhost");
                qsrm.setSenderPort(getLocalPort());
                sendQueryServerRequestMessage(qsrm);
               
                Thread.sleep(3000);
                sendClientRegistrationMessage(new MessageResponseCallback() {
                    @Override
                    public void messageResponseReceived(MessageResponse messageResponse) {
                        System.out.println(messageResponse);
                    }
                });
            } catch (Exception ex) {
                Logger.getLogger(TestClient.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        this.start();
    }

}
