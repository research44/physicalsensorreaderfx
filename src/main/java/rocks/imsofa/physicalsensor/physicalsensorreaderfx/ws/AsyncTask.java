/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws;

import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.ThreadUtil;

/**
 *
 * @author USER
 */
public abstract class AsyncTask {
    protected abstract void run(Callback callback);
    public void start(final Callback callback){
        ThreadUtil.submit(()->{
            run(callback);
        });
    }
    
    public static interface Callback{
        public void success();
        public void failed(Throwable error);
    }
}
