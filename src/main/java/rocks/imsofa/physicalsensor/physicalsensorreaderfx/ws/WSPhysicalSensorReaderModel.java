/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws;

import java.io.File;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.PhysicalSensorReaderModel;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.receiver.InterruptSignalDataReceiver;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.receiver.JepSignalDataReceiver;

/**
 *
 * @author lendle
 */
public class WSPhysicalSensorReaderModel extends PhysicalSensorReaderModel{
    
    public WSPhysicalSensorReaderModel(File projectRootDirectory) throws Exception {
        this.projectRootDirectory=projectRootDirectory;
        initSignalDataReceivers();
    }
    
    /**
     * can override this method to implement ws branch for remote data sources
     * @throws Exception 
     */
    protected InterruptSignalDataReceiver createFrequencySignalDataReceiver(){
        return new InterruptSignalDataReceiver();
    }
    /**
     * can override this method to implement ws branch for remote data sources
     * @throws Exception 
     */
    protected InterruptSignalDataReceiver createVolumesSignalDataReceiver(){
        return new InterruptSignalDataReceiver();
    }
    /**
     * can override this method to implement ws branch for remote data sources
     * @throws Exception 
     */
    protected InterruptSignalDataReceiver createHrSignalDataReceiver(){
        return new InterruptSignalDataReceiver();
    }
    /**
     * can override this method to implement ws branch for remote data sources
     * @throws Exception 
     */
    protected InterruptSignalDataReceiver createGSRSignalDataReceiver(){
        return new InterruptSignalDataReceiver();
    }
    
    /**
     * can override this method to implement ws branch for remote data sources
     * @throws Exception 
     */
    protected void initSignalDataReceivers() throws Exception{
        InterruptSignalDataReceiver frequencyReceiver = createFrequencySignalDataReceiver();
        frequencyReceiver.setId("frequencies");
        receivers.add(frequencyReceiver);

        InterruptSignalDataReceiver volumeReceiver = createVolumesSignalDataReceiver();
        volumeReceiver.setId("volumes");
        receivers.add(volumeReceiver);

        InterruptSignalDataReceiver proxy = (InterruptSignalDataReceiver) createHrSignalDataReceiver();
        proxy.setId("hr");
        
        receivers.add(proxy);
        InterruptSignalDataReceiver proxy2 = (InterruptSignalDataReceiver) createGSRSignalDataReceiver();
        proxy2.setId("test3");
        
        receivers.add(proxy2);
    }
    
}
