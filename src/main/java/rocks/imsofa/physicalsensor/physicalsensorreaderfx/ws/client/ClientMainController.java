/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.client;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.HBox;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalData;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalDataListener;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.SignalDataType;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.receiver.SignalDataReceiver;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.ThreadUtil;

/**
 * FXML Controller class
 *
 * @author lendle
 */
public class ClientMainController implements Initializable, ClientGUICommandEventListener {

    private Client client = null;
    @FXML
    private HBox topContainer;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            client = new Client();
            client.getSession().addDataListener(SignalDataType.TYPE_HR, new SignalDataListener() {
                @Override
                public void newData(SignalDataReceiver source, SignalData data) {
                    Platform.runLater(() -> {
                        labelHRValue.setText("" + (Double) data.getValue());
                    });

                }
            });
            client.getSession().addDataListener(SignalDataType.TYPE_FREQUENCY, new SignalDataListener() {
                @Override
                public void newData(SignalDataReceiver source, SignalData data) {

                    Platform.runLater(() -> {
                        labelFrequencyValue.setText("" + (Double) data.getValue());
                    });
                }
            });
            client.getSession().addDataListener(SignalDataType.TYPE_GSR, new SignalDataListener() {
                @Override
                public void newData(SignalDataReceiver source, SignalData data) {

                    Platform.runLater(() -> {
                        labelGSRValue.setText("" + (Double) data.getValue());
                    });
                }
            });
            client.getSession().addDataListener(SignalDataType.TYPE_VOLUME, new SignalDataListener() {
                @Override
                public void newData(SignalDataReceiver source, SignalData data) {
                    Platform.runLater(() -> {
                        labelVolumeValue.setText("" + (Double) data.getValue());
                    });
                }
            });
            client.getClientContext().addClientStatusChangeListener(new ClientStatusChangeListener() {
                @Override
                public void onStateChanged(ClientStatus oldState, ClientStatus newState) {
                    Platform.runLater(() -> {
                        labelMessage.setText(client.getLocalId());
                    });
                    if (newState.equals(ClientStatus.BOUND_SERVER)) {
                        Platform.runLater(() -> {
                            topContainer.setStyle("-fx-background-color: green");
                        });
                    } else {
                        Platform.runLater(() -> {
                            topContainer.setStyle("-fx-background-color: red");
                        });
                    }
                }
            });
            ThreadUtil.submit(() -> {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ClientMainController.class.getName()).log(Level.SEVERE, null, ex);
                }
                Platform.runLater(() -> {
                    try {
                        client.start();
                        client.addClientGUICommandEventListener(this);
                    } catch (Exception ex) {
                        Logger.getLogger(ClientMainController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
            });

//            client.bindServerAsync();
        } catch (Exception ex) {
            Logger.getLogger(ClientMainController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private Label labelGSRValue;

    @FXML
    private Label labelHRValue;

    @FXML
    private Label labelFrequencyValue;

    @FXML
    private Label labelVolumeValue;

    @FXML
    private Label labelMessage;

    @FXML
    private ProgressIndicator iconBeeper;

    public void shutdown() throws Exception {
        client.stop();
    }

    @Override
    public void commandReceived(ClientGUICommand command) {
        if (command instanceof BeepCommand) {
            Platform.runLater(() -> {
                iconBeeper.setVisible(true);
            });
            ThreadUtil.submit(() -> {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ClientMainController.class.getName()).log(Level.SEVERE, null, ex);
                }
                Platform.runLater(() -> {
                    iconBeeper.setVisible(false);
                });
            });
        } else if (command instanceof RenameCommand) {
            RenameCommand renameCommand = (RenameCommand) command;
            client.writeIdPrefix(renameCommand.getNewName());
        }
    }
}
