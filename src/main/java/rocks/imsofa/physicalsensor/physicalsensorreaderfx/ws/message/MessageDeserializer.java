/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message;

import com.google.gson.Gson;

/**
 *
 * @author USER
 */
public class MessageDeserializer {
    public static Message deserialize(SerializedMessage serializedMessage) throws Exception{
        Gson gson=new Gson();
        return (Message)gson.fromJson(serializedMessage.getJson(), Class.forName(serializedMessage.getType()));
    }
}
