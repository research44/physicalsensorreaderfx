/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.multicast;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.NetworkInterface;
import java.util.logging.Level;
import java.util.logging.Logger;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.NetworkInterfaceUtil;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.PropertiesUtil;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.Constants;

/**
 *
 * @author lendle
 */
public class MulticastUtil {

    public static void multicast(final String groupIP, final int port, final String message) {
        new Thread() {
            public void run() {
                try {
                    //multicast
                    InetAddress group = InetAddress.getByName(groupIP);
                    DatagramPacket packet;
                    byte[] buffer = message.getBytes();
                    packet = new DatagramPacket(buffer, buffer.length, group, port);
                    MulticastSocket socket = new MulticastSocket();
                    String niName = PropertiesUtil.getNetworkInterface();
                    if (niName != null) {
                        Logger.getLogger(MulticastUtil.class.getName()).info("use network interface: "+niName);
                        NetworkInterface ni=NetworkInterfaceUtil.getNetworkInterface(niName);
                        socket.setNetworkInterface(ni);
                    }
                    Logger.getLogger(MulticastSocket.class.getName()).info("send broadcast niName = "+niName+", groupIP="+groupIP);
                    socket.send(packet);
                    socket.close();
                    //DebugUtils.log(dataString);
                } catch (Exception e) {
                    Logger.getLogger(MulticastSocket.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                }
            }
        }.start();
    }

    public static void multicast(final String message) {
        multicast(Constants.MULTICAST_GROUP, Constants.MULTICAST_PORT, message);
    }
}
