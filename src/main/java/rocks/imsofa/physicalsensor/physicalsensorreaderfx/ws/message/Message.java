/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message;

/**
 *
 * @author lendle
 */
public interface Message {
    public String getMessageId();
    public String getSenderId();
    public long getTimestamp();
}
