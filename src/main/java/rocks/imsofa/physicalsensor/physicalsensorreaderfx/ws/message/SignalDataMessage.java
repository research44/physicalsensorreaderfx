/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message;

/**
 *
 * @author lendle
 */
public class SignalDataMessage extends AbstractMessage{
    private double value=-1;
    private int signalType=-1;
    
    public static final int TYPE_FREQUENCY=0, TYPE_VOLUME=1, TYPE_HR=2, TYPE_GSR=3;

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public int getSignalType() {
        return signalType;
    }

    public void setSignalType(int signalType) {
        this.signalType = signalType;
    }
    
    
}
