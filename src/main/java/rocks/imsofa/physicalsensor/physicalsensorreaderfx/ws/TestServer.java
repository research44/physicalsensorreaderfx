/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws;

import rocks.imsofa.physicalsensor.physicalsensorreaderfx.utils.SerializationUtil;

/**
 *
 * @author lendle
 */
public class TestServer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        PhysicalSensorReaderServer server=new PhysicalSensorReaderServer(SerializationUtil.createTempDirectory(), 8080);
        server.getRemoteSessions().addRemoteSessionListener(new RemoteSessions.RemoteSessionListener() {
            @Override
            public void clientRegistered(RemoteSession clientSession) {
                System.out.println(clientSession.getId());
            }
        });
        server.start();
    }

}
