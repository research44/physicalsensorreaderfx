/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Enum.java to edit this template
 */
package rocks.imsofa.physicalsensor.physicalsensorreaderfx;

/**
 *
 * @author lendle
 */
public enum SignalDataType {
    TYPE_FREQUENCY, TYPE_VOLUME, TYPE_HR, TYPE_GSR;
}
