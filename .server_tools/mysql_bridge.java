//DEPS mysql:mysql-connector-java:8.0.33
//DEPS commons-io:commons-io:2.11.0
//DEPS com.google.code.gson:gson:2.10.1
import java.sql.*;
import java.io.*;
import java.util.*;
import org.apache.commons.io.*;
import com.google.gson.*;
import java.math.*;

public class mysql_bridge{
	public static void main(String [] args) throws Exception{
		Class.forName("com.mysql.jdbc.Driver");
		File queueFolder=new File("queue");
		queueFolder.mkdirs();
		GsonBuilder gsonBuilder = new GsonBuilder();  
		gsonBuilder.setLenient();  
		Gson gson = gsonBuilder.create();
		Map<String, String> obsoleteFile=new HashMap<>();
		while(true){
			File [] files=queueFolder.listFiles();
			if(files!=null && files.length>0){
				int length=files.length;
				int processed=0;
				Connection c=DriverManager.getConnection("jdbc:mysql://localhost/most111", "most111", "most111");
				PreparedStatement stmt=c.prepareStatement("insert into signaldatamessage (messageId, senderId, timestamp, dbtimestamp, signalType, value) values (?,?,?,?,?,?)");
				for(File file : files){
					if(obsoleteFile.containsKey(file.getName())){
						processed++;
						continue;
					}
					String json=FileUtils.readFileToString(file, "utf-8");
					Map data=null;
					try{
						data=gson.fromJson(json, Map.class);
					}catch(Exception e){
						json=json.substring(0, json.length()-1);
						try{
							data=gson.fromJson(json, Map.class);
						}catch(Exception e2){
							processed++;
							file.delete();
							System.out.println("skip "+file);
							continue;
						}
					}
					stmt.setString(1, ""+data.get("messageId"));
					stmt.setString(2, ""+data.get("senderId"));
					stmt.setLong(3, new BigDecimal(""+data.get("timestamp")).longValue());
					stmt.setLong(4, System.currentTimeMillis());
					stmt.setInt(5, (int)(Double.valueOf(""+data.get("signalType")).doubleValue()));
					stmt.setDouble(6, Double.valueOf(""+data.get("value")));
					stmt.executeUpdate();
					if(!file.delete()){
						obsoleteFile.put(file.getName(), "");
					}
					processed++;
					System.out.printf("%d/%d\r\n", processed, length);
				}
				c.close();
			}
			System.out.println("done a round");
			Thread.sleep(5000);
		}
	}
}
