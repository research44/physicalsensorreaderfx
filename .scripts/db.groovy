//this is a sample do-nothing script
//a variable "message" refers to the
//captured message
import java.sql.*;
import java.util.*;
import java.lang.reflect.*;
import rocks.imsofa.physicalsensor.physicalsensorreaderfx.ws.message.*;
import org.apache.commons.io.FileUtils;
import com.google.gson.*;
import java.io.*;

void process(Message message){
	SignalDataMessage sdm=(SignalDataMessage)message;
	/*
	//Class driverClass=Class.forName("org.hsqldb.jdbc.JDBCDriver", true, classLoader);
	Class driverClass=Class.forName("com.mysql.jdbc.Driver", true, classLoader);
	Object driver=driverClass.newInstance();
	Method m=driverClass.getMethod("connect", String.class, Properties.class);
	Properties props=new Properties();
	//props.setProperty("user", "sa");
	//props.setProperty("password", "");
	//Connection c=m.invoke(driver, "jdbc:hsqldb:hsql://localhost/xdb", props);
	
	props.setProperty("user", "most111");
	props.setProperty("password", "most111");
	Connection c=m.invoke(driver, "jdbc:mysql://localhost/most111", props);
	
	PreparedStatement stmt=c.prepareStatement("insert into signaldatamessage (messageId, senderId, timestamp, dbtimestamp, signalType, value) values (?,?,?,?,?,?)");

	stmt.setString(1, sdm.getMessageId());
	stmt.setString(2, sdm.getSenderId());
	stmt.setLong(3, sdm.getTimestamp());
	stmt.setLong(4, System.currentTimeMillis());
	stmt.setInt(5, sdm.getSignalType());
	stmt.setDouble(6, sdm.getValue());
	stmt.executeUpdate();
	c.close();
	*/
	Gson gson=new Gson();
	String json=gson.toJson(sdm);
	File queueFolder=new File(".server_tools");
	queueFolder=new File(queueFolder, "queue");
	queueFolder.mkdirs();
	FileUtils.write(new File(queueFolder, ""+System.currentTimeMillis()+".json"), json, "utf-8");
}

