-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- 主機： 127.0.0.1
-- 產生時間： 2023-03-20 05:10:23
-- 伺服器版本： 10.4.22-MariaDB
-- PHP 版本： 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫: `most111`
--
CREATE DATABASE IF NOT EXISTS `most111` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `most111`;

-- --------------------------------------------------------

--
-- 資料表結構 `signaldatamessage`
--

DROP TABLE IF EXISTS `signaldatamessage`;
CREATE TABLE `signaldatamessage` (
  `messageId` varchar(255) NOT NULL,
  `senderId` varchar(255) NOT NULL,
  `timestamp` bigint(20) NOT NULL,
  `dbtimestamp` bigint(20) NOT NULL,
  `signalType` int(11) NOT NULL,
  `value` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `signaldatamessage`
--
ALTER TABLE `signaldatamessage`
  ADD PRIMARY KEY (`messageId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
