import time;
from rocks.imsofa.physicalsensor.physicalsensorreaderfx import JepJavaClassInstanceFactory;
from rocks.imsofa.physicalsensor.physicalsensorreaderfx import SignalData;

proxy=JepJavaClassInstanceFactory.newInstance("rocks.imsofa.physicalsensor.physicalsensorreaderfx.receiver.JepSignalDataReceiver", "test");
while True:
    ts = time.time()*1000
    data=SignalData(int(ts), 123.0);
    proxy.putSignalData(data);
    time.sleep(1);
