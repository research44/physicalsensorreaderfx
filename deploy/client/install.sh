#!/bin/sh
# mkdir -p /home/pi/.config/autostart
# cp physicalsensorreaderfx.desktop /home/pi/.config/autostart
wget -O client_dist.tar https://gitlab.com/research44/physicalsensorreaderfx/-/raw/WS%E9%80%A3%E7%B7%9A/client_dist.tar
mv client_dist.tar ~/Desktop
cd ~/Desktop
tar xvf client_dist.tar
mv deploy/ PhysicalSensorReaderFX/
cd ~/Desktop/PhysicalSensorReaderFX
sudo cp sensor_reader.desktop /etc/xdg/autostart
rm /home/pi/.config/autostart/physicalsensorreaderfx.desktop
sudo cp wpa_supplicant.conf /etc/wpa_supplicant/
