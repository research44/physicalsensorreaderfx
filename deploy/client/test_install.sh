#!/bin/sh
sudo killall java
wget -O client_dist.tar https://gitlab.com/research44/physicalsensorreaderfx/-/raw/3a9d4389e222c7a90a9def94dfb50057fb7df9c0/client_dist.tar
mv client_dist.tar ~/Desktop
cd ~/Desktop
tar xvf client_dist.tar
sudo rm -r -f ~/Desktop/physicalsensorreaderfx
mv deploy/ physicalsensorreaderfx/
cd ~/Desktop/physicalsensorreaderfx
sudo cp sensor_reader.desktop /etc/xdg/autostart
