import time, sys
from grove.adc import ADC
from rocks.imsofa.physicalsensor.physicalsensorreaderfx import JepJavaClassInstanceFactory;
from rocks.imsofa.physicalsensor.physicalsensorreaderfx import SignalData;

proxy=JepJavaClassInstanceFactory.newInstance("rocks.imsofa.physicalsensor.physicalsensorreaderfx.receiver.JepSignalDataReceiver", "gsr");
adc = ADC()
while True:
	ts = time.time()*1000
	value=adc.read(0)
	data=SignalData(int(ts), float(value));
	proxy.putSignalData(data);
	time.sleep(3)
