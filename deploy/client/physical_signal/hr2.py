#!/usr/bin/env python3
from datetime import timedelta
from datetime import datetime
import time
import RPi.GPIO as GPIO
from rocks.imsofa.physicalsensor.physicalsensorreaderfx import JepJavaClassInstanceFactory;
from rocks.imsofa.physicalsensor.physicalsensorreaderfx import SignalData;

proxy=JepJavaClassInstanceFactory.newInstance("rocks.imsofa.physicalsensor.physicalsensorreaderfx.receiver.JepSignalDataReceiver", "hr");

# Connect the Grove Ear Clip Sensor to digital port D16
sensorPin = 3
start_time = datetime.now()
counter = 0
temp = [0] * 21
data_effect = True
heart_rate = 0
max_heartpulse_duty = 5000
BUTTON_GPIO = 16

def millis():
   global start_time
   dt = datetime.now() - start_time
   ms = (dt.days * 24 * 60 * 60 + dt.seconds) * 1000 + dt.microseconds / 1000.0

   return ms

def arrayInit():
    global temp
    global counter
    counter = 0
    temp = [0] * 21
    temp[20] = millis()

def sum():                               
    global heart_rate, temp, data_effect
    if data_effect:
        heart_rate=1200000/(temp[20]-temp[0]);
    
def interrupt():
    global counter
    global temp
    global data_effect
    print("interrupt "+str(counter))
    
    if counter ==21 and data_effect:
        for i in range(20):
            #shift
            temp[i]=temp[i+1]
            counter=20
    temp[counter] = millis()
    if counter == 0:       
        sub = temp[counter]-temp[20]
    else:
        sub = temp[counter]-temp[counter-1]

    if sub > max_heartpulse_duty:
        data_effect = False
        counter = 0
        arrayInit()
    if counter == 20 and data_effect:
        #counter = 0
        counter+=1
        sum()
    elif counter != 20 and data_effect:
        counter += 1
    else: 
        counter = 0
        data_effect = True

def main():

    global heart_rate
    
    print("Please place the sensor correctly")
    time.sleep(4)
    print("Starting measurement...")
    arrayInit()
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(BUTTON_GPIO, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    while True:
        GPIO.wait_for_edge(BUTTON_GPIO, GPIO.RISING)
        value = GPIO.input(BUTTON_GPIO)
        if value > 0:
            interrupt()
        time.sleep(0.73)
        ts = time.time()*1000
        data=SignalData(int(ts), float(heart_rate));
        proxy.putSignalData(data);
        print("HR: {:2.0f}".format(heart_rate))
     
 
if __name__ == '__main__':
    main()
